
public class Robot{

    EstadoRobot suspendido;
    EstadoRobot atendiendo;
    EstadoRobot caminando;
    EstadoRobot cocinando;

    EstadoRobot estadoActual;

    public Robot(){
        suspendido = new RobotSuspendido(this);
        atendiendo = new RobotAtendiendo(this);
        caminando = new RobotCaminando(this);
        cocinando = new RobotCocinando(this);

        estadoActual = suspendido;
    }

    public void asignarEstadoRobot(EstadoRobot nuevoEstado){
        estadoActual = nuevoEstado;
    }

    public void suspender(){
        estadoActual.robotSuspendido();
    }

    public void atender(){
        estadoActual.robotAtendiendo();
    }

    public void caminar(){
        estadoActual.robotCaminando();
    }

    public void cocinar(){
        estadoActual.robotCocinando();
    }

    public EstadoRobot getEstadoSuspendido(){
        return suspendido;
    }

    public EstadoRobot getEstadoAtendiendo(){
        return atendiendo;
    }

    public EstadoRobot getEstadoCaminando(){
        return caminando;
    }

    public EstadoRobot getEstadoCocinando(){
        return cocinando;
    }
}
