import java.util.Iterator;

public class RobotCocinando implements EstadoRobot{

    Robot robot;
    Menu menu = new Menu();
    Coccion preparacion = new Opciones();

    public RobotCocinando(Robot nuevoRobot){
        robot = nuevoRobot;
    }

    public void robotSuspendido(){
        System.out.println("No me puedo suspender, estoy cocinando.");
    }

    public void robotAtendiendo(){
        System.out.println("No puedo tomar una orden, estoy cocinando.");
    }

    public void robotCaminando(){
        System.out.println("Estoy cocinando, no puedo caminar.");
    }

    public void robotCocinando(){
        //Aquí el robot cocinará.
        //Avisa que ya cocino y se suspende
        cocinarHamburguesa();
        robot.asignarEstadoRobot(robot.getEstadoSuspendido());
    }

    //public Hamburguesa buscarHamburguesa(){

    //}

    //public void cocinarHamburguesa(Hamburguesa eleccionDelUsuario){
    public void cocinarHamburguesa(){
        Iterator menuEspecial = menu.getIteradorMenuEspecial();
        Iterator menuGerente = menu.getIteradorMenuGerente();
        Iterador menuFijo = menu.getIteradorMenuFijo();
        while(menuEspecial.hasNext()){
            //if(((Hamburguesa)menuEspecial.next()) == (eleccionDelUsuario)){
            if(((Hamburguesa)menuEspecial.next()).getQueso() &&
                   ((Hamburguesa)menuEspecial.next()).getVegetariana()){
                    //Aquí llamo el template adecuado
                    preparacion.coccion();
                }
            //}
        }
        while(menuGerente.hasNext()){
            //if(((Hamburguesa)menuGerente.next()) == (eleccionDelUsuario)){
                if(((Hamburguesa)menuGerente.next()).getQueso() &&
                   ((Hamburguesa)menuGerente.next()).getVegetariana()){
                    //Aquí llamo el template adecuado
                    preparacion.coccion();
                }
            //}
        }
        while (menuFijo.hasNext()) {
            //if(((Hamburguesa)menuFijo.next()) == (eleccionDelUsuario)){
                if(((Hamburguesa)menuFijo.next()).getQueso() &&
                   ((Hamburguesa)menuFijo.next()).getVegetariana()){
                    //Aquí llamo el template adecuado
                    preparacion.coccion();
                }
            //}
        }
    }
}
