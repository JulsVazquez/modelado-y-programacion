
public abstract class Coccion {

    public boolean tieneQueso = false;
    public boolean esVegetariana = false;

    /**
     * Método abstracto para que cada clase lo modifique a su antojo.
     */
    abstract void queso();

    abstract void carne();

    /**
     * Método template (plantilla)
     */
    public final void coccion(){
        pan();
        mayonesa();
        mostaza();
        vegetales();
        catsup();
        pan2();
    }

    void pan(){
        System.out.println("Estoy poniendo el pan.");
    }

    void mayonesa(){
        System.out.println("Estoy poniendo la mayonesa.");
    }

    void mostaza(){
        System.out.println("Estoy poniendo la mostaza.");
    }

    void vegetales(){
        System.out.println("Estoy poniendo jitomate, cebolla y lechuga.");
    }

    void catsup(){
        System.out.println("Estoy poniendo catsup.");
    }

    void pan2(){
        System.out.println("Estoy poniendo el pan");
    }
}
