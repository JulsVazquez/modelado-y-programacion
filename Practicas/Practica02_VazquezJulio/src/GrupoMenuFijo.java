/**
 * Clase para las hamburguesas que son un Arreglo.
 */

public class GrupoMenuFijo {
    private Hamburguesa hamburguesas[] = new Hamburguesa[3];

    public GrupoMenuFijo(){
        hamburguesas[0] = new Hamburguesa(7620, "Hendrix", "Carne de res, " +
                                          "queso oaxaca, chiles habaneros " +
                                          "y chorizo.", 100, true, false);
        hamburguesas[1] = new Hamburguesa(7641, "Tae", "Pollo, queso cheddar " +
                                          "y salsa BBQ.", 89, true, false);
        hamburguesas[2] = new Hamburguesa(7621, "Casi-classic", "Pepinillos, " +
                                          "aros de cebolla, carne de res y " +
                                          "queso cheddar.", 120, true, false);
    }

    public Iterador getIterador(){
        return new IteradorArreglo();
    }

    public class IteradorArreglo implements Iterador{

        int indice;

        @Override
        public boolean hasNext(){
            if(indice < hamburguesas.length){
                return true;
            }
            return false;
        }

        @Override
        public Object next(){
            if(this.hasNext()){
                return hamburguesas[indice++];
            }
            return null;
        }
    }
}
