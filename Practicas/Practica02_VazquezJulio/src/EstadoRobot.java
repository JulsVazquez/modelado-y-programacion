/**
 * Interfaz EstadoRobot, la cuál nos dará las acciones y estados principales del
 * robot.
 */

public interface EstadoRobot {
    void robotSuspendido();
    void robotAtendiendo();
    void robotCaminando();
    void robotCocinando();
}
