import java.util.Iterator;

public class RobotCaminando implements EstadoRobot{

    Robot robot;
    Menu menu = new Menu();

    Iterator menuEspecial = menu.getIteradorMenuEspecial();
    Iterator menuGerente = menu.getIteradorMenuGerente();
    Iterador menuFijo = menu.getIteradorMenuFijo();

    public RobotCaminando(Robot nuevoRobot){
        robot = nuevoRobot;
    }

    public void robotSuspendido(){
        //Aquí tenemos que suspender al robot
        System.out.println("Suspendiendo Robot..." +
                           "\n Robot suspendido.");
        robot.asignarEstadoRobot(robot.getEstadoSuspendido());
    }

    public void robotAtendiendo(){
        //Aquí tenemos que mandar a atender al cliente
        robot.asignarEstadoRobot(robot.getEstadoAtendiendo());
    }

    public void robotCaminando(){
        //Aqui el robot camina hasta el cliente
        System.out.println("Ya estoy contigo, no puedo caminar.");
        robot.asignarEstadoRobot(robot.getEstadoAtendiendo());
    }

    public void robotCocinando(){
        System.out.println("No puedo cocinar, estoy caminando." +
                           "\n ¡Eso es peligroso!");
    }
}
