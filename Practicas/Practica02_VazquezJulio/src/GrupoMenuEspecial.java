import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Iterator;

public class GrupoMenuEspecial {

    private Hashtable<Integer, Hamburguesa> menuTabla = new Hashtable<>();
    private Queue<Hamburguesa> hamburguesas = new LinkedList<>();


    public GrupoMenuEspecial(){
        Hamburguesa asiEstaBien = new Hamburguesa(6630,"Así está bien",
                                         "Portobello, cebolla caramelizada,\n" +
                                         "mayonesa de tofu y queso tipo " +
                                         "mozzarella", 140, true, true);
        Hamburguesa tresQuesos = new Hamburguesa(6639, "3 quesos",
                                                 "Carne de soya tipo res, " +
                                                 "verduras, tempura,\nqueso " +
                                                 "tipo mozzarella, queso " +
                                                 "tipo manchego (de soya)\ny " +
                                                 "aderezo untable tipo " +
                                                 "queso crema.", 170, true,
                                                 true);
        Hamburguesa unPocoDeTodo = new Hamburguesa(6636,"Un poco de todo",
                                                   "Tofu empanizado, queso " +
                                                   "tipo manchego,\njitomate " +
                                                   "las más finas hierbas " +
                                                   "chimichurri, cacahuate\n" +
                                                   "tostado y aderezo tipo " +
                                                   "ranch.", 185, true, true);
        menuTabla.put(1,asiEstaBien);
        menuTabla.put(2,tresQuesos);
        menuTabla.put(3,unPocoDeTodo);
    }

    public Iterator getIterator(){
        return menuTabla.values().iterator();
    }
}
