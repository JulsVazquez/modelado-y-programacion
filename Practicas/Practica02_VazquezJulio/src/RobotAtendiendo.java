import java.util.Iterator;

public class RobotAtendiendo implements EstadoRobot{

    Robot robot;
    Menu menu = new Menu();
    Coccion preparacion = new Opciones();

    public RobotAtendiendo(Robot nuevoRobot){
        robot = nuevoRobot;
    }

    public void robotSuspendido(){
        System.out.println("No me puedes suspender, te estoy atendiendo.");
    }

    public void robotAtendiendo(){
    Iterator menuEspecial = menu.getIteradorMenuEspecial();
    Iterator menuGerente = menu.getIteradorMenuGerente();
    Iterador menuFijo = menu.getIteradorMenuFijo();
    System.out.println("Ya te estoy atendiendo. Este es el menú: \n");
        while(menuEspecial.hasNext()){
            System.out.println(((Hamburguesa)menuEspecial.next()).toString());
        }
        while (menuGerente.hasNext()) {
            System.out.println(((Hamburguesa)menuGerente.next()).toString());
        }
        while (menuFijo.hasNext()) {
            System.out.println(((Hamburguesa)menuFijo.next()).toString());
        }
        robot.asignarEstadoRobot(robot.getEstadoCocinando());
    }

    public void robotCaminando(){
        System.out.println("No puedo caminar, te estoy atendiendo.");
    }

    public void robotCocinando(){
        //Aquí el robot pasará a cocinar
        robot.asignarEstadoRobot(robot.getEstadoCocinando());
        System.out.println("Procederé a cocinar tu comida");
    }
}
