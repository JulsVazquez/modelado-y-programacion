import java.util.Iterator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Robot robot = new Robot();
        Menu menu = new Menu();
        Iterator menuEspecial = menu.getIteradorMenuEspecial();
        Iterator menuGerente = menu.getIteradorMenuGerente();
        Iterador menuFijo = menu.getIteradorMenuFijo();
        Coccion preparacion = new Opciones();

        Scanner sc = new Scanner(System.in);
        int opcion;
        int idHamburguesa;

        System.out.println("Bienvenido, soy R. Daneel Olivaw.");
        System.out.println("Estado actual: Suspendido");
        System.out.println("Para poder atenderlo necesito caminar a su lugar");
            System.out.println("Ingrese una de las siguientes " +
                               "opciones para poder atenderlo: \n\n" +
                               "1. Suspender\n"+
                               "2. Caminar\n" +
                               "3. Atender\n" +
                               "4. Cocinando\n" +
                               "0.- Salir");
            do{
                System.out.println("Puedes escribir:");
                String opcionUsuario = sc.nextLine();
                opcion = Integer.parseInt(opcionUsuario);

                switch(opcion){
                case 1:
                    robot.suspender();
                    break;

                case 2:
                    robot.caminar();
                    break;
                case 3:
                    robot.atender();
                    System.out.println("Elige el ID:");
                    String opcionID = sc.nextLine();
                    idHamburguesa = Integer.parseInt(opcionID);
                    switch(idHamburguesa){
                        case 7621:
                            robot.cocinar();
                            break;
                        case 6630:
                            robot.cocinar();
                            break;
                        case 6639:
                            robot.cocinar();
                            break;
                        case 6636:
                            robot.cocinar();
                            break;
                        case 7628:
                            robot.cocinar();
                            break;
                        case 7527:
                            robot.cocinar();
                            break;
                        case 6537:
                            robot.cocinar();
                            break;
                        case 7620:
                            robot.cocinar();
                            break;
                        case 7641:
                            robot.cocinar();
                            break;
                        default:
                            System.out.println("No elegiste un ID válido, presiona 3 para ver el menú de nuevo.");
                            break;
                    }
                    break;
                case 4:
                    robot.cocinar();
                    break;
                case 0:
                    System.out.println("\nHASTA LUEGO, VUELVA PRONTO\n");
                    break;

                default:
                    System.out.println("Elige una opción válida.\n");
                    break;
                }
            } while (opcion != 0);
    }
}
