import java.util.ArrayList;
import java.util.Iterator;

public class GrupoMenuGerente {
    private ArrayList<Hamburguesa> hamburguesas = new ArrayList<>();

    public GrupoMenuGerente(){
        Hamburguesa roller = new Hamburguesa(7628, "Roller",
                                             "Carne de res, tocino," +
                                             "champiñones y queso " +
                                             "manchego.", 100, true, false);
        Hamburguesa tex = new Hamburguesa(7527, "Tex", "Chilli, carne de res" +
                                          " y pepinillos.", 110, false, false);
        Hamburguesa garden = new Hamburguesa(6537, "Garden", "Pepinillos, " +
                                             "tortita de lenteja, mayonesa " +
                                             "de aguacate y jitomate.", 135,
                                             false, true);
        hamburguesas.add(roller);
        hamburguesas.add(tex);
        hamburguesas.add(garden);
    }

    public Iterator getIterator(){
        return hamburguesas.iterator();
    }


}
