
public class RobotSuspendido implements EstadoRobot{

    Robot robot;

    public RobotSuspendido(Robot nuevoRobot){
        robot = nuevoRobot;
    }

    public void robotSuspendido(){
        System.out.println("Zzzzzz... Ya estoy suspendido.");
    }

    public void robotAtendiendo(){
        System.out.println("Zzzzzz... No puedo atenderte, estoy suspendido.");
    }

    public void robotCaminando(){
        //Aquí va código
        System.out.println("Estoy caminando hacía a ti para atenderte.");
        System.out.println("Para antenderte presiona 3");
        robot.asignarEstadoRobot(robot.getEstadoAtendiendo());
    }

    public void robotCocinando(){
        System.out.println("Zzzzzz... No puedo cocinarte, estoy suspendido.");
    }
}
