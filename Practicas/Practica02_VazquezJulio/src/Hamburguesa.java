
public class Hamburguesa {
    private int idHamburguesa;
    private String nombreHamburguesa;
    private String descripcionHamburguesa;
    private int precioHamburguesa;
    private boolean tieneQueso;
    private boolean esVegetariana;

    public Hamburguesa(int idHamburguesa, String nombreHamburguesa,
                       String descripcionHamburguesa, int precioHamburguesa,
                       boolean tieneQueso, boolean esVegetariana){
        this.idHamburguesa = idHamburguesa;
        this.nombreHamburguesa = nombreHamburguesa;
        this.descripcionHamburguesa = descripcionHamburguesa;
        this.precioHamburguesa = precioHamburguesa;
        this.tieneQueso = tieneQueso;
        this.esVegetariana = esVegetariana;
    }

    public String toString(){
        return String.format("\n\nID : %04d\n" +
                             "Nombre : %s\n" +
                             "Descripción : %s\n" +
                             "Precio : $%d\n" +
                             "¿Tiene queso? : %b\n" +
                             "¿Es vegetariana? : %b\n\n", idHamburguesa,
                             nombreHamburguesa, descripcionHamburguesa,
                             precioHamburguesa, tieneQueso, esVegetariana);
    }

    public boolean getQueso(){
        return tieneQueso;
    }

    public boolean getVegetariana(){
        return esVegetariana;
    }

    public int getID(){
        return idHamburguesa;
    }
}
