import java.util.Iterator;

public class Menu {
    private GrupoMenuEspecial menuEspecial;
    private GrupoMenuFijo menuFijo;
    private GrupoMenuGerente menuGerente;

    public Menu(){
        menuEspecial = new GrupoMenuEspecial();
        menuFijo = new GrupoMenuFijo();
        menuGerente = new GrupoMenuGerente();
    }

    public Iterator getIteradorMenuEspecial(){
        return menuEspecial.getIterator();
    }

    public Iterator getIteradorMenuGerente(){
        return menuGerente.getIterator();
    }

    public Iterador getIteradorMenuFijo(){
        return menuFijo.getIterador();
    }
}
