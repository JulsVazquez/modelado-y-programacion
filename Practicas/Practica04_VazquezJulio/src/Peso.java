/**
 * Clase Peso
 * La clase nos mostrará la cualidad que será el peso.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class Peso implements Cualidad{
    @Override
    public String mostrarCualidad(){
        return "Peso";
    }
}
