/**
 * Clase Reforzado
 * La clase nos da los elementos de la cualidad reforzado que tiene un blindaje
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class Simple extends Blindaje{
    @Override
    public String nombre(){
        return "Blindaje Simple";
    }
    @Override
    public String descripcion(){
        return "Un blindaje de nave simple.";
    }
    @Override
    public double precio(){
        return 12.42;
    }
    @Override
    public int nivel(){
        return 5;
    }
}
