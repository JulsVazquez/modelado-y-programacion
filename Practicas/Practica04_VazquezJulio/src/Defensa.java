/**
 * Clase Defensa
 * La clase nos mostrará la cualidad que será la defensa.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class Defensa implements Cualidad{
    @Override
    public String mostrarCualidad(){
        return "Defensa";
    }
}
