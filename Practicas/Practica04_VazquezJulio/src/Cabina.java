/**
 * Clase Cabina
 * La clase nos dará las cualidades de la cabina, como su precio y su
 * nivel.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public abstract class Cabina implements Componente{
    private Cualidad cualidad = new Peso();
    @Override
    public Cualidad getCualidad(){
        return cualidad;
    }
    @Override
    public abstract double precio();
    @Override
    public abstract int nivel();
}
