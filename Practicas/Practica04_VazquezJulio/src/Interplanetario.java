/**
 * Clase Interplanetario
 * La clase nos da los elementos de la cualidad interplanetario que tiene un
 * propulsor.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class Interplanetario extends Propulsion{
    @Override
    public String nombre(){
        return "Viaje interplanetario";
    }
    @Override
    public String descripcion(){
        return "Propulsor con la capacidad de realizar viajes interplanetarios en la Vía Lactea.";
    }
    @Override
    public double precio(){
        return 22.49;
    }
    @Override
    public int nivel(){
        return 10;
    }
}
