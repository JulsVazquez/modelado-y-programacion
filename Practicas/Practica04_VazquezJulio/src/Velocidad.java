/**
 * Clase Velocidad
 * La clase nos mostrará la cualidad que será la velocidad.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class Velocidad implements Cualidad{
    @Override
    public String mostrarCualidad(){
        return "Velocidad";
    }
}
