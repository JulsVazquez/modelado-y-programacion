/**
 * Clase Intercontinental
 * La clase nos da los elementos de la cualidad intercontinental que tiene un
 * propulsor.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class Intercontinental extends Propulsion{
    @Override
    public String nombre(){
        return "Viaje intercontinental";
    }
    @Override
    public String descripcion(){
        return "Propulsor con la capacidad de realizar viajes intercontinentales";
    }
    @Override
    public double precio(){
        return 10.99;
    }
    @Override
    public int nivel(){
        return 4;
    }
}
