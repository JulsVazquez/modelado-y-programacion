/**
 * Interfaz Componente
 * La interfaz nos sobrecargará los componentes de cuálquier cualidad que pueda
 * tener la nave.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public interface Componente {
    /**
     * Método nombre
     * @return el nombre del componente de la nave
     */
    public String nombre();

    /**
     * Método getCualidad
     * @return la cualidad de la nave
     */
    public Cualidad getCualidad();

    /**
     * Método descripcion
     * @return la descripción del componente de la nave.
     */
    public String descripcion();

    /**
     * Método precio()
     * @return el precio del componente de la nave.
     */
    public double precio();

    /**
     * Método nivel()
     * @return el nivel que otorga el componente de la nave.
     */
    public int nivel();
}
