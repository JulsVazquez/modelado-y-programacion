/**
 * Clase Reforzado
 * La clase nos da los elementos de la cualidad reforzado que tiene un blindaje
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class Reforzado extends Blindaje{
    @Override
    public String nombre(){
        return "Blindaje Reforzado";
    }
    @Override
    public String descripcion(){
        return "Un blindaje reforzado para naves.";
    }
    @Override
    public double precio(){
        return 23.12;
    }
    @Override
    public int nivel(){
        return 9;
    }
}
