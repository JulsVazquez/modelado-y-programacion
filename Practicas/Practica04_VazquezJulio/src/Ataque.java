/**
 * Clase Ataque
 * La clase nos mostrará la cualidad que será el ataque.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class Ataque implements Cualidad{
    @Override
    public String mostrarCualidad(){
        return "Ataque";
    }
}
