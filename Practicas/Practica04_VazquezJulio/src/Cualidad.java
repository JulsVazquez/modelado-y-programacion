/**
 * InterfazCualidad
 * La interfaz nos ayudará a mostrar las cualidades de cada nave. Este método
 * se herederá a las clases que otorguen una cualidad a la nave.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public interface Cualidad {
    /**
     * Método mostrarCualidad
     * @return cualidad. Regresa la cualidad que se le otoroga a un componente
     * de la nave.
     */
    public String mostrarCualidad();
}
