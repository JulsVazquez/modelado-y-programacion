/**
 * Clase Ejercito
 * La clase nos da los elementos de la cualidad ejercito que tiene una cabina
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class Ejercito extends Cabina{
    @Override
    public String nombre(){
        return "Cabina para ejército";
    }
    @Override
    public String descripcion(){
        return "Cabina para la entrada de todo un ejército.";
    }
    @Override
    public double precio(){
        return 35.25;
    }
    @Override
    public int nivel(){
        return 15;
    }
}
