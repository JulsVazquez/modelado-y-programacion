/**
 * Clase Tripulacion
 * La clase nos da los elementos de la cualidad tripulacion que tiene una cabina
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class Tripulacion extends Cabina{
    @Override
    public String nombre(){
        return "Cabina para una tripulación";
    }
    @Override
    public String descripcion(){
        return "Cabina de buen tamaño que permite a una tripulación pequeña.";
    }
    @Override
    public double precio(){
        return 22.31;
    }
    @Override
    public int nivel(){
        return 10;
    }
}
