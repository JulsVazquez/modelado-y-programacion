/**
 * Clase Nave
 * La clase construira lo esencial para mostrar una nave así como para poder
 * desplegar el precio de sus componentes.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
import java.util.LinkedList;

public class Nave {
    private LinkedList<Componente> componentes =
        new LinkedList<>();

    /**
     * Método agregaComponente
     * El método irá agregando un componente a la lista ligada del mismo
     */
    public void agregaComponente(Componente com){
        componentes.add(com);
    }

    /**
     * Método obtenCosto
     * El método nos regresará el costo de cada componente que se encuentra en
     * la lista ligada
     * @return costo. El costo de cada componente.
     */
    public double obtenCosto(){
        double costo = 0.0;
        for (Componente com : componentes) {
            costo += com.precio();
        }
        return costo;
    }

    /**
     * Método muestraNave()
     * El método nos mostrará cada componente, cualidad y precio que se ha ido
     * eligiendo en la nave.
     */
    public void muestraNave(){
        for (Componente com : componentes) {
            System.out.print("Componente: " + com.nombre());
            System.out.print("\nCualidad: " +
                             com.getCualidad().mostrarCualidad() +
                             ": " + com.nivel());
            System.out.println("\nPrecio: " + com.precio());
        }

    }
}
