/**
 * Clase Fortaleza
 * La clase nos da los elementos de la cualidad fortaleza que tiene un blindaje
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class Fortaleza extends Blindaje{
    @Override
    public String nombre(){
        return "Blindaje Fortaleza";
    }
    @Override
    public String descripcion(){
        return "El blindaje más resistente de la galaxia.";
    }
    @Override
    public double precio(){
        return 32.76;
    }
    @Override
    public int nivel(){
        return 19;
    }
}
