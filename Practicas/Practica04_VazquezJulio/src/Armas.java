/**
 * Clase Armas
 * La clase armas nos dará las cualidades de las armas, como su precio y su
 * nivel.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public abstract class Armas implements Componente{
    private Cualidad cualidad = new Ataque();
    @Override
    public Cualidad getCualidad(){
        return cualidad;
    }
    @Override
    public abstract double precio();
    @Override
    public abstract int nivel();
}
