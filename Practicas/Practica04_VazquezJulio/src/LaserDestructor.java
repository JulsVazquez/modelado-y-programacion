/**
 * Clase LaserDestructor
 * La clase nos da los elementos de la cualidad laser destructor que tiene un
 * arma
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class LaserDestructor extends Armas{
    @Override
    public String nombre(){
        return "Laser Destructor de Mundos";
    }
    @Override
    public String descripcion(){
        return "Un rayo laser capaz de destruir un planeta";
    }
    @Override
    public double precio(){
        return 90.21;
    }
    @Override
    public int nivel(){
        return 39;
    }
}
