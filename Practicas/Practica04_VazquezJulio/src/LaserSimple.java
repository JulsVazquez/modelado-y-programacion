/**
 * Clase LaserSimple
 * La clase nos da los elementos de la cualidad laser simple que tiene un arma
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class LaserSimple extends Armas{
    @Override
    public String nombre(){
        return "Laser Simple";
    }
    @Override
    public String descripcion(){
        return "Un rayo laser simple.";
    }
    @Override
    public double precio(){
        return 21.43;
    }
    @Override
    public int nivel(){
        return 10;
    }
}
