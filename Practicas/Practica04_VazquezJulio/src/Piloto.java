/**
 * Clase Piloto
 * La clase nos da los elementos de la cualidad piloto que tiene una cabina
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class Piloto extends Cabina{
    @Override
    public String nombre(){
        return "Cabina para un piloto";
    }
    @Override
    public String descripcion(){
        return "Cabina estándar donde se permite un piloto";
    }
    @Override
    public double precio(){
        return 6.99;
    }
    @Override
    public int nivel(){
        return 2;
    }
}
