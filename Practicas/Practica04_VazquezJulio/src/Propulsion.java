/**
 * Clase Propulsion
 * La clase nos dará las cualidades del propulsor, como su precio y su
 * nivel.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public abstract class Propulsion implements Componente{
    private Cualidad cualidad = new Velocidad();
    @Override
    public Cualidad getCualidad(){
        return cualidad;
    }
    @Override
    public abstract double precio();
    @Override
    public abstract int nivel();
}
