/**
 * Clase Intergalactico
 * La clase nos da los elementos de la cualidad intergalactico que tiene un
 * propulsor.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class Intergalactico extends Propulsion{
    @Override
    public String nombre(){
        return "Viaje Intergalactico";
    }
    @Override
    public String descripcion(){
        return "Propulsor con la capacidad de realizar viajes intergalacticos en la constelación";
    }
    @Override
    public double precio(){
        return 44.10;
    }
    @Override
    public int nivel(){
        return 20;
    }
}
