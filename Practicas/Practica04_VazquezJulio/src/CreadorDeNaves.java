/**
 * Clase CreadorDeNaves
 * La clase nos ayudará a probar el programa creador de naves.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
import java.util.Scanner;

public class CreadorDeNaves {
    public static void main(String[] args) {
        CreadorDeNaves menu = new CreadorDeNaves();
        menu.correr();
    }
    public void correr(){
        encabezado();
        accionCategoria();
    }

    private void encabezado(){
        System.out.println("+---------------------------------------------+");
        System.out.println("|        Bienvenido al Creador de Naves       |");
        System.out.println("+---------------------------------------------+");
    }

    private void accionCategoria(){
        double presupuesto;
        Builder builder = new Builder();
        String propulsor = "";
        String blindaje = "";
        String arma = "";
        Nave nave;
        String cabina = "";
        Scanner sc = new Scanner(System.in);
        int opcion;
        System.out.println("Ingresa tu presupuesto:");
        String opcionUsuario = sc.nextLine();
        presupuesto = Double.parseDouble(opcionUsuario);
        System.out.println("Elige uno:");
        System.out.println("1. Cabina");
        System.out.println("2. Sistema de Propulsión");
        System.out.println("3. Blindaje");
        System.out.println("4. Armas");
        System.out.println("0. Salir");
        opcionUsuario = sc.nextLine();
        opcion = Integer.parseInt(opcionUsuario);
        switch(opcion){
        case 0:
            System.out.println("¡¡¡HASTA LUEGO!!!");
            System.exit(-1);
            break;
        case 1:
            menuCabina();
            cabina = seleccionCabina(seleccion());

            menuPropulsion();
            propulsor = seleccionPropulsor(seleccion());

            menuBlindaje();
            blindaje = seleccionBlindaje(seleccion());

            menuArmas();
            arma = seleccionArma(seleccion());


            nave = builder.preparaNave(cabina,propulsor,blindaje,arma);

            if (nave.obtenCosto() > presupuesto) {
                System.out.println("No te alcanza");
                System.out.println("Tengo las siguientes naves predeterminadas:\n"+
                                   "1. Nave individual de combate\n"+
                                   "2. Nave militar de transporte\n"+
                                   "3. Base espacial de guerra");
                Nave naveIndividual = builder.preparaNave("piloto","intercontinental","simple","misiles");
                Nave naveMilitar = builder.preparaNave("ejercito","interplanetario","reforzado","laser simple");
                Nave baseMilitar = builder.preparaNave("tripulacion","intergalactico","fortaleza","laser destructor");
                opcionUsuario = sc.nextLine();
                opcion = Integer.parseInt(opcionUsuario);
                if (opcion == 1 && naveIndividual.obtenCosto() > presupuesto) {
                    System.out.println("No te alcanza");
                    System.exit(-1);
                } else {
                System.out.println("\n\nNave Lista");
                naveIndividual.muestraNave();
                }
                if (opcion == 2 && naveMilitar.obtenCosto() > presupuesto){
                    System.out.println("No te alcanza");
                    System.exit(-1);
                } else {
                System.out.println("\n\nNave Lista");
                naveMilitar.muestraNave();
                }
                if(opcion == 3 && baseMilitar.obtenCosto() > presupuesto){
                    System.out.println("No te alcanza");
                    System.exit(-1);
                } else {
                System.out.println("\n\nNave Lista");
                baseMilitar.muestraNave();
                }
            } else{
                System.out.println("\n\n\nNave Lista");
                nave.muestraNave();
                System.out.println("Total: " + nave.obtenCosto());
            }
            break;
        case 2:
            menuPropulsion();
            propulsor = seleccionPropulsor(seleccion());

            menuCabina();
            cabina = seleccionCabina(seleccion());

            menuBlindaje();
            blindaje = seleccionBlindaje(seleccion());

            menuArmas();
            arma = seleccionArma(seleccion());

            nave = builder.preparaNave(cabina,propulsor,blindaje,arma);

            if (nave.obtenCosto() > presupuesto) {
                System.out.println("No te alcanza");
                System.out.println("Tengo las siguientes naves predeterminadas:\n"+
                                   "1. Nave individual de combate\n"+
                                   "2. Nave militar de transporte\n"+
                                   "3. Base espacial de guerra");
                Nave naveIndividual = builder.preparaNave("piloto","intercontinental","simple","misiles");
                Nave naveMilitar = builder.preparaNave("ejercito","interplanetario","reforzado","laser simple");
                Nave baseMilitar = builder.preparaNave("tripulacion","intergalactico","fortaleza","laser destructor");
                opcionUsuario = sc.nextLine();
                opcion = Integer.parseInt(opcionUsuario);
                if (opcion == 1 && naveIndividual.obtenCosto() > presupuesto) {
                    System.out.println("No te alcanza");
                    System.exit(-1);
                } else{
                System.out.println("\n\nNave Lista");
                naveIndividual.muestraNave();
                }
                if (opcion == 2 && naveMilitar.obtenCosto() > presupuesto){
                    System.out.println("No te alcanza");
                    System.exit(-1);
                } else{
                System.out.println("\n\nNave Lista");
                naveMilitar.muestraNave();
                }
                if(opcion == 3 && baseMilitar.obtenCosto() > presupuesto){
                    System.out.println("No te alcanza");
                    System.exit(-1);
                } else{
                System.out.println("\n\nNave Lista");
                baseMilitar.muestraNave();
                }
            } else{
                System.out.println("\n\n\nNave Lista");
                nave.muestraNave();
                System.out.println("Total: " + nave.obtenCosto());
            }
            break;
        case 3:
            menuBlindaje();
            blindaje = seleccionBlindaje(seleccion());

            menuPropulsion();
            propulsor = seleccionPropulsor(seleccion());

            menuCabina();
            cabina = seleccionCabina(seleccion());

            menuArmas();
            arma = seleccionArma(seleccion());

            nave = builder.preparaNave(cabina,propulsor,blindaje,arma);

            if (nave.obtenCosto() > presupuesto) {
                System.out.println("No te alcanza");
                System.out.println("Tengo las siguientes naves predeterminadas:\n"+
                                   "1. Nave individual de combate\n"+
                                   "2. Nave militar de transporte\n"+
                                   "3. Base espacial de guerra");
                Nave naveIndividual = builder.preparaNave("piloto","intercontinental","simple","misiles");
                Nave naveMilitar = builder.preparaNave("ejercito","interplanetario","reforzado","laser simple");
                Nave baseMilitar = builder.preparaNave("tripulacion","intergalactico","fortaleza","laser destructor");
                opcionUsuario = sc.nextLine();
                opcion = Integer.parseInt(opcionUsuario);
                if (opcion == 1 && naveIndividual.obtenCosto() > presupuesto) {
                    System.out.println("No te alcanza");
                    System.exit(-1);
                } else{
                System.out.println("\n\nNave Lista");
                naveIndividual.muestraNave();
                }
                if (opcion == 2 && naveMilitar.obtenCosto() > presupuesto){
                    System.out.println("No te alcanza");
                    System.exit(-1);
                } else{
                System.out.println("\n\nNave Lista");
                naveMilitar.muestraNave();
                }
                if(opcion == 3 && baseMilitar.obtenCosto() > presupuesto){
                    System.out.println("No te alcanza");
                    System.exit(-1);
                } else{
                System.out.println("\n\nNave Lista");
                baseMilitar.muestraNave();
                }
            } else{
                System.out.println("\n\n\nNave Lista");
                nave.muestraNave();
                System.out.println("Total: " + nave.obtenCosto());
            }
            break;
        case 4:
            menuArmas();
            arma = seleccionArma(seleccion());

            menuBlindaje();
            blindaje = seleccionBlindaje(seleccion());

            menuPropulsion();
            propulsor = seleccionPropulsor(seleccion());

            menuCabina();
            cabina = seleccionCabina(seleccion());


            nave = builder.preparaNave(cabina,propulsor,blindaje,arma);

            if (nave.obtenCosto() > presupuesto) {
                System.out.println("No te alcanza");
                System.out.println("Tengo las siguientes naves predeterminadas:\n"+
                                   "1. Nave individual de combate\n"+
                                   "2. Nave militar de transporte\n"+
                                   "3. Base espacial de  guerra");
                seleccion();
            } else{
                System.out.println("\n\n\nNave Lista");
                nave.muestraNave();
                System.out.println("Total: " + nave.obtenCosto());
            }
            break;
        default:
            System.out.println("Un error inesperado ha sucedido.");
            break;
        }
    }

    private String seleccionCabina(int opcion){
        String cabina = "";
        switch(opcion){
        case 1:
            cabina = "piloto";
            break;
        case 2:
            cabina = "tripulacion";
            break;
        case 3:
            cabina = "ejercito";
            break;
        default:
            System.out.println("Un error inesperado ha sucedido");
            break;
        }
        return cabina;
    }


    private String seleccionPropulsor(int opcion){
        String propulsor = "";
        switch(opcion){
        case 1:
            propulsor = "intercontinental";
            break;
        case 2:
            propulsor = "interplanetario";
            break;
        case 3:
            propulsor = "intergalactico";
            break;
        default:
            System.out.println("Un error inesperado ha sucedido");
            break;
        }
        return propulsor;
    }


    private String seleccionBlindaje(int opcion){
        String blindaje = "";
        switch(opcion){
        case 1:
            blindaje = "simple";
            break;
        case 2:
            blindaje = "reforzado";
            break;
        case 3:
            blindaje = "fortaleza";
            break;
        default:
            System.out.println("Un error inesperado ha sucedido");
            break;
        }
        return blindaje;
    }


    private String seleccionArma(int opcion){
        String arma = "";
        switch(opcion){
        case 1:
            arma = "laser simple";
            break;
        case 2:
            arma = "misiles";
            break;
        case 3:
            arma = "laser destructor";
            break;
        default:
            System.out.println("Un error inesperado ha sucedido");
            break;
        }
        return arma;
    }

    private int seleccion(){
        Scanner kb = new Scanner(System.in);
        int opcion = -1;
        try {
            System.out.println("Elige tu opción:");
            String usuario = kb.nextLine();
            opcion = Integer.parseInt(usuario);
        } catch (NumberFormatException e) {
            System.out.println("Selección inválida.");
        }
        return opcion;
    }

    private void menuCabina(){
        System.out.println("Elige uno:");
        System.out.println("1. Un Piloto");
        System.out.println("2. Tripulación pequeña");
        System.out.println("3. Ejército");
    }

    private void menuPropulsion(){
        System.out.println("Elige uno:");
        System.out.println("1. Intercontinental");
        System.out.println("2. Interplanetario");
        System.out.println("3. Intergalactico");
    }

    private void menuBlindaje(){
        System.out.println("Elige uno:");
        System.out.println("1. Simple");
        System.out.println("2. Reforzado");
        System.out.println("3. Fortaleza");
    }

    private void menuArmas(){
        System.out.println("Elige uno:");
        System.out.println("1. Láser Simple");
        System.out.println("2. Misiles");
        System.out.println("3. Láser Destructor");
    }
    /**
     * Nave aElegir = builder.preparaNave("piloto", "intercontinental", "fortaleza", "lasersimple");
     * System.out.println("Nave preparada");
     * aElegir.muestraNave();
     * System.out.println("Total: " + aElegir.obtenCosto());
     */
}
