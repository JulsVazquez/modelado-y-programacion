/**
 * Clase Builder
 * La clase Builder nos ayudará a construir una nave con su respectiva forma
 * siguiendo el patrón Builder.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public class Builder {
    /**
     * Método preparaNave
     * El método nos ayudará a preparar cualquier tipo de nave.
     * @param cabina. La cabina de la nave.
     * @param propulsor. El propulsor de la nave.
     * @param blindaje. El blindaje de la nave,
     * @param armas. La arma de la nave.
     * @return nave. La nave creada.
     */
    public Nave preparaNave(String cabina, String propulsor, String blindaje,
                            String armas){
        Nave nave = new Nave();

        // Primera etapa para la cabina
        if (cabina.equalsIgnoreCase("piloto")) {
            nave.agregaComponente(new Piloto());
        } else if (cabina.equalsIgnoreCase("tripulacion")) {
            nave.agregaComponente(new Tripulacion());
        } else if (cabina.equalsIgnoreCase("ejercito")) {
            nave.agregaComponente(new Ejercito());
        }

        // Segunda etapa para el propulsor
        if (propulsor.equalsIgnoreCase("intercontinental")) {
            nave.agregaComponente(new Intercontinental());
        } else if (propulsor.equalsIgnoreCase("interplanetario")) {
            nave.agregaComponente(new Interplanetario());
        } else if (propulsor.equalsIgnoreCase("intergalactico")) {
            nave.agregaComponente(new Intergalactico());
        }

        // Tercera etapa para el blindaje
        if (blindaje.equalsIgnoreCase("simple")) {
            nave.agregaComponente(new Simple());
        } else if (blindaje.equalsIgnoreCase("reforzado")) {
            nave.agregaComponente(new Reforzado());
        } else if (blindaje.equalsIgnoreCase("fortaleza")) {
            nave.agregaComponente(new Fortaleza());
        }

        // Cuarta etapa para las armas
        if (armas.equalsIgnoreCase("laser simple")) {
            nave.agregaComponente(new LaserSimple());
        } else if (armas.equalsIgnoreCase("laser destructor")) {
            nave.agregaComponente(new LaserDestructor());
        } else if (armas.equalsIgnoreCase("misiles")) {
            nave.agregaComponente(new Misiles());
        }
        return nave;
    }
}
