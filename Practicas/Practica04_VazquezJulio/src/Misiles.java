/**
 *
 *
 */
public class Misiles extends Armas{
    /**
     *
     *
     */
    @Override
    public String nombre(){
        return "Misiles";
    }

    /**
     *
     *
     */
    @Override
    public String descripcion(){
        return "Un paquete de misiles.";
    }

    /**
     *
     *
     */
    @Override
    public double precio(){
        return 45.26;
    }

    /**
     *
     *
     */
    @Override
    public int nivel(){
        return 22;
    }
}
