/**
 * Clase Blindaje
 * La clase nos dará las cualidades del blindaje, como su precio y su
 * nivel.
 * @author Julio Vázquez
 * @version 1.0
 * @date 27-Jul-2021
 */
public abstract class Blindaje implements Componente{
    private Cualidad cualidad = new Defensa();
    @Override
    public Cualidad getCualidad(){
        return cualidad;
    }
    @Override
    public abstract double precio();
    @Override
    public abstract int nivel();
}
