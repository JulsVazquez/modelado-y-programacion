Notas de la Práctica
====================

### El WaySub

El restaurante de baguettes WaySub tiene un menú personalizable para sus 
productos. En este menú, seleccionas:
* Tipo de pan
* Ingredientes

Dependiendo del tipo de pan y de cuantos ingredientes va a tener, se le va 
aumentando el precio.

Ingredientes:
* Pan \$10.00
* Pollo \$29.99
* Pepperoni \$29.99
* Jamón \$24.99
* Lechuga \$9.99
* Jitomate \$7.99
* Cebolla \$7.99
* Mostaza \$2.99
* Catsup \$2.99
* Mayonesa \$2.99

Se requiere imprimir la _descripción de la baguette_ en el ticket y el _precio 
total_.

Los ingredientes pueden pedirse doble o triple, con lo que la descripción del 
pedido imprimirá el número de cantidades que se pidió.

Ejemplo: Si pide doble pepperoni se imprime "pepperoni, pepperoni" y se le 
cobran los dos ingredientes.

Precios a mi elección.

**Nota:**_Esta parte de la práctica se hace con el patron Decorator._
_La clase a decorar es el pan, entonces creo distintos panes en sus clases para 
definirlos._


### Comprando cadenas de pizzas

El restaurante decide absorber la cadena de pizzas _"Las pizzas de don 
cangrejo"_ y venderlos en la misma sucursal.

La cadena de pizzas ya tienen métodos que te dicen: _el tipo de ingrediente que
tiene cada pizza, uno para el tipo de queso (chedar o manchego), el tipo de 
carne (salchicha, jamón, pollo) y el tipo de masa (masa gruesa, masa delgada), 
además de un precio exacto de las pizzas._ 

Ingredientes Pizza:
* Queso: Chedar o Manchego
* Carne: Salchicha, Jamón o Pollo.
* Masa: Gruesa o Delgada

El ticket tiene que ser estándar. Debe tener una decripción y el precio. 

Sólo se venden 5 tipos de pizza fija (combinando el tipo de ingredientes).
Los tipos de pizza dependerán de cada uno de nosotros.

Tipos de Pizza:
* Pizza Carnes Frias: Chedar, Salchicha, Jamon, Pollo, Masa Gruesa. \$299.99
* Pizza QuesiPollo: Manchego, Salchicha, Pollo, Masa Delgada. \$249.99
* Pizza Quesitos: Chedar, Manchego, Jamon, Masa Delgada. \$199.99
* Pizza Piernas:Chedar Jamon, Salchicha, Masa Gruesa. \149.99
* Pizza Sencilla: Chedar, Jamon, Masa Delgada. \109.99

### Prueba
Realizar un main interactivo, preguntar si queremos un baguette o una pizza.

En el caso del baguette te deje agregar tantos ingredientes como quieras hasta 
que le digas que está lista. 

En el caso de las pizzas te de una lista de las pizzas con sus precios y sólo te
deje elegir una.


**Nota:**_Podemos usar una interfaz y una clase abstracta de forma 
intercambiable, dependiendo de cual sirva más a la situación._
