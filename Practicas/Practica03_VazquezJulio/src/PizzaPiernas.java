/**
 * Clase PizzaPiernas
 * La clase modelará una Pizza con queso cheddar, jamon, salchicha, masa gruesa.
 */

public class PizzaPiernas extends Pizza{
    static final String NOMBRE = "Pizza Piernas";
    static final String QUESO = "Queso Cheddar";
    static final String CARNE = "Salchicha, Jamon";
    static final String MASA = "Masa gruesa";
    static final double COSTO = 149.99;

    public PizzaPiernas(){
        super(NOMBRE,QUESO,CARNE,MASA,COSTO);
    }
}
