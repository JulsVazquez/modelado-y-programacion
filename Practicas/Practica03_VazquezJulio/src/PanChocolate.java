/**
 * Clase PanChocolate.
 * La clase creará un Pan con Chocolate con su nombre y su costo.
 */

public class PanChocolate extends Baguettes{

    /**
     * Método constructor()
     * Define el nombre del Pan para Baguette con Chocolate.
     */
    public PanChocolate() {
        descripcion = "Pan con Chocolate";
    }

    /**
     * Método heredado costo()
     * Regresa el costo del pan con chocolate.
     * @return el costo del pan con chocolate.
     */
    public double costo() {
        return 20.99;
    }
}
