/**
 * Clase PizzaConSalchicha
 * La clase modelará una Pizza con salchicha, queso manchego y masa delgada.
 */

public class PizzaConSalchicha extends Pizza{
    static final String NOMBRE = "Pizza de Carnes";
    static final String QUESO = "Queso Manchego";
    static final String CARNE = "Salchicha, Jamon, Pollo";
    static final String MASA = "Masa gruesa";
    static final double COSTO = 299.99;

    public PizzaConSalchicha(){
        super(NOMBRE,QUESO,CARNE,MASA,COSTO);
    }
}
