/**
 * Clase PanIntegral
 * La clase creará un Pan Integral con su nombre y su costo.
 */

public class PanIntegral extends Baguettes{

    /**
     * Método constructor()
     * Define el nombre del Pan Integral para Baguette.
     */
    public PanIntegral() {
        descripcion = "Pan Integral";
    }

    /**
     * Método heredado costo()
     * Regresa el costo del pan integral.
     * @return el costo del pan integral.
     */
    public double costo() {
        return 15.99;
    }
}
