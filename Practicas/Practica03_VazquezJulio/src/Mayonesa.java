/**
 * Clase Mayonesa.
 * La clase extiende de IngredienteDecorador para que moldee una mayonesa como
 * un ingrediente extra a agregar a la baguette.
 * @version 1.0
 * @date 14-Jul-2021
 * @author Julio Vázquez
 */

public class Mayonesa extends IngredienteDecorador {
    Baguettes baguette;

    /**
     * Método constructor de la clase.
     */
    public Mayonesa(Baguettes baguette) {
        this.baguette = baguette;
    }

    /**
     * Método heredado getDescripcion()
     * Regresa la Baguette decorada con Mayonesa.
     * @return la descripción del Baguette con Mayonesa.
     */
    public String getDescripcion() {
        return baguette.getDescripcion() + ", Mayonesa";
    }

    /**
     * Método heredado costo()
     * Regresa el costo de la mayonesa.
     * @return el costo de la mayonesa.
     */
    public double costo() {
        return 2.99 + baguette.costo();
    }
}
