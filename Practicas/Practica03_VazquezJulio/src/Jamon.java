/**
 * Clase Jamon.
 * La clase extiende de IngredienteDecorador para que moldee un jamón como un
 * ingrediente extra a agregar a la baguette.
 * @version 1.0
 * @date 14-Jul-2021
 * @author Julio Vázquez
 */

public class Jamon extends IngredienteDecorador {
    Baguettes baguette;

    /**
     * Método constructor de la clase.
     */
    public Jamon(Baguettes baguette) {
        this.baguette = baguette;
    }

    /**
     * Método heredado getDescripcion()
     * Regresa la Baguette decorada con Jamón.
     * @return la descripción del Baguette con Jamón.
     */
    public String getDescripcion() {
        return baguette.getDescripcion() + ", Jamón";
    }

    /**
     * Método heredado costo()
     * Regresa el costo del jamón.
     * @return el costo del jamón.
     */
    public double costo() {
        return 24.99 + baguette.costo();
    }
}
