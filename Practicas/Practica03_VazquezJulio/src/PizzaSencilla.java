/**
 * Clase PizzaSencilla
 * La clase modelará una Pizza con cheddar, jamon y masa delgada.
 */

public class PizzaSencilla extends Pizza{
    static final String NOMBRE = "Pizza Sencilla";
    static final String QUESO = "Queso Cheddar";
    static final String CARNE = "Jamon";
    static final String MASA = "Masa delgada";
    static final double COSTO = 109.99;

    public PizzaSencilla(){
        super(NOMBRE,QUESO,CARNE,MASA,COSTO);
    }
}
