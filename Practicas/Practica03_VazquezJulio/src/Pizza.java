/**
 * Clase Pizza
 * La clase modelará una Pizza, está será una Pizza que tendrá la siguiente
 * forma siempre: Queso, Carne y Masa.
 */

public class Pizza implements InterfazPizza {

    String nombre;
    String queso;
    String carne;
    String masa;
    double precio;

    /**
     * Constructor de la clase
     */
    public Pizza(String nombre, String queso, String carne, String masa,
                 double precio){
        this.nombre = nombre;
        this.queso = queso;
        this.carne = carne;
        this.masa = masa;
        this.precio = precio;
    }

    @Override
    public String getQueso(){
        return queso;
    }

    @Override
    public String getCarne(){
        return carne;
    }

    @Override
    public String getMasa(){
        return masa;
    }

    @Override
    public String getNombre(){
        return nombre;
    }

    @Override
    public double getPrecio(){
        return precio;
    }

    @Override
    public void setPrecio(double precio){
        this.precio = precio;
    }

    @Override
    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    @Override
    public void setQueso(String queso){
        this.queso = queso;
    }

    @Override
    public void setCarne(String carne){
        this.carne = carne;
    }

    @Override
    public void setMasa(String masa){
        this.masa = masa;
    }

    @Override
    public String toString(){
        return String.format("%s: %s, %s, %s. $%2.2f", nombre, queso, carne, masa,
                             precio);
    }

}
