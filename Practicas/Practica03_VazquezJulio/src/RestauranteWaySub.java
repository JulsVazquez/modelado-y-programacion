/**
 * Clase RestauranteWaySub
 * La clase representará la clase principal en donde se integrarán todos los
 * métodos que hemos programado anteriormente para simular el restaurante
 * Way Sub.
 * @version 1.0
 * @date 14-Jul-2021
 * @author Julio Vázquez
 */
import java.util.Scanner;

public class RestauranteWaySub {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int opcion;
        System.out.println("BIENVENIDO A WAYSUB\n\n" +
                           "¿Qué deseas ordenar?\n" +
                           "1. Baguette \n" +
                           "2. Pizza \n");
        String opcionUsuario = sc.nextLine();
        opcion = Integer.parseInt(opcionUsuario);
            switch(opcion){
            case 1:
                System.out.println("¿Qué tipo de pan quieres?\n" +
                                   "1. Pan Sencillo.\n" +
                                   "2. Pan Integral.\n" +
                                   "3. Pan Chocolate.\n");
                opcionUsuario = sc.nextLine();
                opcion = Integer.parseInt(opcionUsuario);
                if(opcion == 1){
                    Baguettes panSencillo = new PanSencillo();
                    System.out.println("¿Qué deseas agregar?\n" +
                                       "1. Pollo\n" +
                                       "2. Pepperoni\n" +
                                       "3. Jamón\n"+
                                       "4. Lechuga\n" +
                                       "5. Jitomate\n" +
                                       "6. Cebolla\n" +
                                       "7. Mostaza\n" +
                                       "8. Catsup\n" +
                                       "9. Mayonesa\n");
                    System.out.println("Presiona 0 para terminar");
                    do{
                        opcionUsuario = sc.nextLine();
                        opcion = Integer.parseInt(opcionUsuario);
                        switch(opcion){
                        case 0:
                            break;
                        case 1:
                            panSencillo = new Pollo(panSencillo);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 2:
                            panSencillo = new Pepperoni(panSencillo);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 3:
                            panSencillo = new Jamon(panSencillo);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 4:
                            panSencillo = new Lechuga(panSencillo);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 5:
                            panSencillo = new Jitomate(panSencillo);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 6:
                            panSencillo = new Cebolla(panSencillo);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 7:
                            panSencillo = new Mostaza(panSencillo);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 8:
                            panSencillo = new Catsup(panSencillo);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 9:
                            panSencillo = new Mayonesa(panSencillo);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        default:
                            System.out.println("Esa no es una opción válida");
                            System.exit(-1);
                        }
                    } while(opcion != 0);
                        System.out.println(panSencillo.getDescripcion() + " $" +
                                           panSencillo.costo());
                        System.exit(-1);
                } else if(opcion == 2){
                    Baguettes panIntegral = new PanIntegral();
                    System.out.println("¿Qué deseas agregar?\n" +
                                       "1. Pollo\n" +
                                       "2. Pepperoni\n" +
                                       "3. Jamón\n"+
                                       "4. Lechuga\n" +
                                       "5. Jitomate\n" +
                                       "6. Cebolla\n" +
                                       "7. Mostaza\n" +
                                       "8. Catsup\n" +
                                       "9. Mayonesa\n");
                     System.out.println("Presiona 0 para terminar");
                    do{
                        opcionUsuario = sc.nextLine();
                        opcion = Integer.parseInt(opcionUsuario);
                        switch(opcion){
                        case 0:
                            break;
                        case 1:
                            panIntegral = new Pollo(panIntegral);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 2:
                            panIntegral = new Pepperoni(panIntegral);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 3:
                            panIntegral = new Jamon(panIntegral);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 4:
                            panIntegral = new Lechuga(panIntegral);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 5:
                            panIntegral = new Jitomate(panIntegral);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 6:
                            panIntegral = new Cebolla(panIntegral);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 7:
                            panIntegral = new Mostaza(panIntegral);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 8:
                            panIntegral = new Catsup(panIntegral);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 9:
                            panIntegral = new Mayonesa(panIntegral);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        default:
                            System.out.println("Esa no es una opción válida");
                            System.exit(-1);
                        }
                    } while(opcion != 0);
                        System.out.println(panIntegral.getDescripcion() + " $" +
                                           panIntegral.costo());
                        System.exit(-1);
                } else if(opcion == 3){
                    Baguettes panChocolate = new PanChocolate();
                    System.out.println("¿Qué deseas agregar?\n" +
                                       "1. Pollo\n" +
                                       "2. Pepperoni\n" +
                                       "3. Jamón\n"+
                                       "4. Lechuga\n" +
                                       "5. Jitomate\n" +
                                       "6. Cebolla\n" +
                                       "7. Mostaza\n" +
                                       "8. Catsup\n" +
                                       "9. Mayonesa\n");
                    System.out.println("Presiona 0 para terminar");
                    do{
                        opcionUsuario = sc.nextLine();
                        opcion = Integer.parseInt(opcionUsuario);
                        switch(opcion){
                        case 0:
                            break;
                        case 1:
                            panChocolate = new Pollo(panChocolate);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 2:
                            panChocolate = new Pepperoni(panChocolate);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 3:
                            panChocolate = new Jamon(panChocolate);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 4:
                            panChocolate = new Lechuga(panChocolate);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 5:
                            panChocolate = new Jitomate(panChocolate);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 6:
                            panChocolate = new Cebolla(panChocolate);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 7:
                            panChocolate = new Mostaza(panChocolate);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 8:
                            panChocolate = new Catsup(panChocolate);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        case 9:
                            panChocolate = new Mayonesa(panChocolate);
                            System.out.println("¿Deseas agregar algo más?");
                            break;
                        default:
                            System.out.println("Esa no es una opción válida");
                            System.exit(-1);
                        }
                    } while(opcion != 0);
                        System.out.println(panChocolate.getDescripcion() + " $" +
                                           panChocolate.costo());
                        System.exit(-1);
                } else
                    System.err.println("Opción inválida");
                System.exit(-1);
                break;
            case 2:
                System.out.println("Elige una pizza:\n" +
                                   "1. Pizza Carnes Frias. $299.99\n" +
                                   "2. Pizza QuesiPollo. $249.99\n" +
                                   "3. Pizza Quesitos. $199.99\n" +
                                   "4. Pizza Piernas. $149.99\n" +
                                   "5. Pizza Sencilla. 109.99\n");
                    opcionUsuario = sc.nextLine();
                    opcion = Integer.parseInt(opcionUsuario);
                if(opcion == 1){
                    Pizza pizzaCarnesFrias = new PizzaConSalchicha();
                    Baguettes pizzaCarnesFriasAdaptada = new PizzaAdapter(pizzaCarnesFrias);
                    System.out.println(pizzaCarnesFriasAdaptada.getDescripcion() + " $" +
                                       pizzaCarnesFriasAdaptada.costo());
                } else if(opcion == 2){
                    Pizza pizzaQuesiPollo = new PizzaQuesiPollo();
                    Baguettes pizzaQuesiPolloAdaptada = new PizzaAdapter(pizzaQuesiPollo);
                    System.out.println(pizzaQuesiPolloAdaptada.getDescripcion() + " $" +
                                       pizzaQuesiPolloAdaptada.costo());
                } else if(opcion == 3){
                    Pizza pizzaQuesitos = new PizzaQuesitos();
                    Baguettes pizzaQuesitosAdaptada = new PizzaAdapter(pizzaQuesitos);
                    System.out.println(pizzaQuesitosAdaptada.getDescripcion() + " $" +
                                       pizzaQuesitosAdaptada.costo());
                } else if(opcion == 4){
                    Pizza pizzaPiernas = new PizzaPiernas();
                    Baguettes pizzaPiernasAdaptada = new PizzaAdapter(pizzaPiernas);
                    System.out.println(pizzaPiernasAdaptada.getDescripcion() + " $" +
                                       pizzaPiernasAdaptada.costo());
                } else if(opcion == 5){
                    Pizza pizzaSencilla = new PizzaSencilla();
                    Baguettes pizzaSencillaAdaptada = new PizzaAdapter(pizzaSencilla);
                    System.out.println(pizzaSencillaAdaptada.getDescripcion() + " $" +
                                       pizzaSencillaAdaptada.costo());
                } else {
                    System.err.println("Opción inválida.");
                    System.exit(-1);
                }
                break;
            default:
                System.out.println("Error, no es una opción válida.");
                System.exit(-1);
            }
    }
}
