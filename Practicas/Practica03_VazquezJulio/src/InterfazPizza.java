/**
 * InterfazPizza
 * La interfaz nos dará todo lo que se necesita para poder construir una pizza.
 * @date 15-Jul-2021
 * @version 1.0
 * @author Julio Vázquez Alvarez
 */

public interface InterfazPizza {

    /**
     * Método setQueso
     * Asignamos el queso de la pizza.
     * @return el queso de la pizza.
     *
     */
    public void setQueso(String queso);

    /**
     * Método setCarne
     * Asignamos la carne a la pizza.
     * @return la carne de la pizza.
     *
     */
    public void setCarne(String carne);

    /**
     * Método setMasa
     * Asignamos la masa de la pizza.
     * @return la masa de la pizza.
     */
    public void setMasa(String masa);

    /**
     * Método setNombre
     * Asignamos el nombre de la pizza.
     * @return el nombre de la pizza.
     */
    public void setNombre(String nombre);

    /**
     * Método setPrecio
     * Asignamos el precio de la pizza.
     * @return el precio de la pizza.
     */
    public void setPrecio(double precio);

    /**
     * Método getPrecio
     * Obtiene el precio de una pizza.
     */
    public double getPrecio();

    /**
     * Método getNombre
     * Obtiene el nombre de una pizza.
     */
    public String getNombre();

    /**
     * Método getQueso
     * Obtiene el queso de una pizza.
     */
    public String getQueso();

    /**
     * Método getCarne
     * Obtiene la carne de la pizza.
     */
    public String getCarne();

    /**
     * Método getMasa
     * Obtiene la masa de la pizza.
     */
    public String getMasa();

    /**
     * Método toString
     * Regresa el formato de cómo se imprimirán los datos de la pizza.
     * @return formato de una pizza.
     */
    public String toString();
}
