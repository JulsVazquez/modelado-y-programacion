/**
 * Clase PizzaAdapter
 * Clase para modelar el patron Adapter y poder agregar las pizzas a la clase
 * Bagguete
 */

public class PizzaAdapter extends Baguettes{
    public Pizza pizza;

    public PizzaAdapter(Pizza pizza){
        this.pizza = pizza;
    }

    @Override
    public String getDescripcion(){
        return String.format("%s: %s, %s, %s.", pizza.getNombre(), pizza.getQueso(),
                             pizza.getCarne(),pizza.getMasa());
    }

    @Override
    public double costo(){
        return pizza.getPrecio();
    }
}
