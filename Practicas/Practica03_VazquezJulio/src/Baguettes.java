/**
 * Clase abstracta Baguette.
 * La clase moldeará una Baguette para poder usar decoradores en ella.
 * @date 14-Jul-2021
 * @version 1.0
 * @author Julio Vázquez Alvarez
 */

public abstract class Baguettes {
    String descripcion = "Baguette desconocida";

    /**
     * Método getDescripcion()
     * El método nos regreserá la descripción decorada de las Baguettes.
     * @return la descripción de la Baguette.
     */
    public String getDescripcion(){
        return descripcion;
    }

    /**
     * Método abstracto costo()
     * El método será heredado para poder devolver el costo total de una
     * Baguette decorada.
     */
    public abstract double costo();
}
