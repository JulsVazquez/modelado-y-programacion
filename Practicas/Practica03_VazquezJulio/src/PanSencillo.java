/**
 * Clase PanSencillo.
 * La clase creará un Pan Sencillo con su nombre y su costo.
 */

public class PanSencillo extends Baguettes{

    public PanSencillo() {
        descripcion = "Pan Sencillo";
    }

    public double costo() {
        return 10.99;
    }
}
