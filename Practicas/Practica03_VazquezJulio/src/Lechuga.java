/**
 * Clase Lechuga.
 * La clase extiende de IngredienteDecorador para que moldee una lechuga como un
 * ingrediente extra a agregar a la baguette.
 * @version 1.0
 * @date 14-Jul-2021
 * @author Julio Vázquez
 */

public class Lechuga extends IngredienteDecorador {
    Baguettes baguette;

    /**
     * Método constructor de la clase.
     */
    public Lechuga(Baguettes baguette) {
        this.baguette = baguette;
    }

    /**
     * Método heredado getDescripcion()
     * Regresa la Baguette decorada con Lechuga.
     * @return la descripción del Baguette con Lechuga.
     */
    public String getDescripcion() {
        return baguette.getDescripcion() + ", Lechuga";
    }

    /**
     * Método heredado costo()
     * Regresa el costo de la lechuga.
     * @return el costo de la lechuga.
     */
    public double costo() {
        return 9.99 + baguette.costo();
    }
}
