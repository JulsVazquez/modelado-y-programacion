/**
 * Clase Catsup.
 * La clase extiende de IngredienteDecorador para que moldee una catsup como un
 * ingrediente extra a agregar a la baguette.
 * @version 1.0
 * @date 14-Jul-2021
 * @author Julio Vázquez
 */

public class Catsup extends IngredienteDecorador {
    Baguettes baguette;

    /**
     * Método constructor de la clase.
     */
    public Catsup(Baguettes baguette) {
        this.baguette = baguette;
    }

    /**
     * Método heredado getDescripcion()
     * Regresa la Baguette decorada con Catsup.
     * @return la descripción del Baguette con Catsup.
     */
    public String getDescripcion() {
        return baguette.getDescripcion() + ", Catsup";
    }

    /**
     * Método heredado costo()
     * Regresa el costo de la catsup.
     * @return el costo de la catsup.
     */
    public double costo() {
        return 2.99 + baguette.costo();
    }
}
