/**
 * Clase PizzaQuesiPollo
 * La clase modelará una Pizza con Manchego, Salchicha, Pollo, Masa Delgada.
 */

public class PizzaQuesiPollo extends Pizza{
    static final String NOMBRE = "Pizza QUesiPollo";
    static final String QUESO = "Queso Manchego";
    static final String CARNE = "Salchicha, Pollo";
    static final String MASA = "Masa delgada";
    static final double COSTO = 249.99;

    public PizzaQuesiPollo(){
        super(NOMBRE,QUESO,CARNE,MASA,COSTO);
    }
}
