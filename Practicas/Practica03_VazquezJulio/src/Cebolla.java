/**
 * Clase Cebolla.
 * La clase extiende de IngredienteDecorador para que moldee una cebolla como un
 * ingrediente extra a agregar a la baguette.
 * @version 1.0
 * @date 14-Jul-2021
 * @author Julio Vázquez
 */

public class Cebolla extends IngredienteDecorador {
    Baguettes baguette;

    /**
     * Método constructor de la clase.
     */
    public Cebolla(Baguettes baguette) {
        this.baguette = baguette;
    }

    /**
     * Método heredado getDescripcion()
     * Regresa la Baguette decorada con Cebolla.
     * @return la descripción del Baguette con Cebolla.
     */
    public String getDescripcion() {
        return baguette.getDescripcion() + ", Cebolla";
    }

    /**
     * Método heredado costo()
     * Regresa el costo de la cebolla.
     * @return el costo de la cebolla.
     */
    public double costo() {
        return 7.99 + baguette.costo();
    }
}
