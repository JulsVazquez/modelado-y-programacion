/**
 * Clase Pollo.
 * La clase extiende de IngredienteDecorador para que moldee un pollo como un
 * ingrediente extra a agregar a la baguette.
 * @version 1.0
 * @date 14-Jul-2021
 * @author Julio Vázquez
 */


public class Pollo extends IngredienteDecorador {
    Baguettes baguette;

    /**
     * Método constructor de la clase.
     */
    public Pollo(Baguettes baguette) {
        this.baguette = baguette;
    }

    /**
     * Método heredado getDescripcion()
     * Regresa la Baguette decorada con Pollo.
     * @return la descripción del Baguette con Pollo.
     */
    public String getDescripcion() {
        return baguette.getDescripcion() + ", Pollo";
    }

    /**
     * Método heredado costo()
     * Regresa el costo del pollo.
     * @return el costo del pollo.
     */
    public double costo() {
        return 29.99 + baguette.costo();
    }
}
