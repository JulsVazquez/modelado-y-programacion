/**
 * Clase Jitomate.
 * La clase extiende de IngredienteDecorador para que moldee un jitomate como un
 * ingrediente extra a agregar a la baguette.
 * @version 1.0
 * @date 14-Jul-2021
 * @author Julio Vázquez
 */

public class Jitomate extends IngredienteDecorador {
    Baguettes baguette;

    /**
     * Método constructor de la clase.
     */
    public Jitomate(Baguettes baguette) {
        this.baguette = baguette;
    }

    /**
     * Método heredado getDescripcion()
     * Regresa la Baguette decorada con Jitomate.
     * @return la descripción del Baguette con Jitomate.
     */
    public String getDescripcion() {
        return baguette.getDescripcion() + ", Jitomate";
    }

    /**
     * Método heredado costo()
     * Regresa el costo de la jitomate.
     * @return el costo de la jitomate.
     */
    public double costo() {
        return 7.99 + baguette.costo();
    }
}
