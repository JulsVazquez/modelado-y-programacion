/**
 * Clase Mostaza.
 * La clase extiende de IngredienteDecorador para que moldee una mostaza como un
 * ingrediente extra a agregar a la baguette.
 * @version 1.0
 * @date 14-Jul-2021
 * @author Julio Vázquez
 */

public class Mostaza extends IngredienteDecorador {
    Baguettes baguette;

    /**
     * Método constructor de la clase.
     */
    public Mostaza(Baguettes baguette) {
        this.baguette = baguette;
    }

    /**
     * Método heredado getDescripcion()
     * Regresa la Baguette decorada con Mostaza.
     * @return la descripción del Baguette con Mostaza.
     */
    public String getDescripcion() {
        return baguette.getDescripcion() + ", Mostaza";
    }

    /**
     * Método heredado costo()
     * Regresa el costo de la mostaza.
     * @return el costo de la mostaza.
     */
    public double costo() {
        return 2.99 + baguette.costo();
    }
}
