/**
 * Clase Pepperoni.
 * La clase extiende de IngredienteDecorador para que moldee un pepperoni como
 * un ingrediente extra a agregar a la baguette.
 * @version 1.0
 * @date 14-Jul-2021
 * @author Julio Vázquez
 */


public class Pepperoni extends IngredienteDecorador {
    Baguettes baguette;

    /**
     * Método constructor de la clase.
     */
    public Pepperoni(Baguettes baguette) {
        this.baguette = baguette;
    }

    /**
     * Método heredado getDescripcion()
     * Regresa la Baguette decorada con Pepperoni.
     * @return la descripción del Baguette con Pepperoni.
     */
    public String getDescripcion() {
        return baguette.getDescripcion() + ", Pepperoni";
    }

    /**
     * Método heredado costo()
     * Regresa el costo del pepperoni
     * @return el costo del pepperoni
     */
    public double costo() {
        return 29.99 + baguette.costo();
    }
}
