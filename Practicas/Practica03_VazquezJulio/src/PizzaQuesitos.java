/**
 * Clase PizzaQuesitos
 * La clase modelará una Pizza con cheddar, queso manchego, jamon y masa delgada.
 */

public class PizzaQuesitos extends Pizza{
    static final String NOMBRE = "Pizza Quesitos";
    static final String QUESO = "Queso Manchego, Queso Cheddar";
    static final String CARNE = "Jamon";
    static final String MASA = "Masa delgada";
    static final double COSTO = 199.99;

    public PizzaQuesitos(){
        super(NOMBRE,QUESO,CARNE,MASA,COSTO);
    }
}
