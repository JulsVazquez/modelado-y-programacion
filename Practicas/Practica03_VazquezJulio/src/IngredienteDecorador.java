/**
 * Clase abstracta IngredienteDecorador.
 * La clase abstracta extenderá a Baguettes para poder obtener la descripción de
 * cada una de las baguettes.
 */

public abstract class IngredienteDecorador extends Baguettes{

    /**
     * Método abstracto getDescripcion()
     * El método sobrecargar la descripción de la clase Baguettes ya que será
     * la descripción a decorar.
     * Todos los ingredientes deben extender a esta clase.
     */
    @Override
    public abstract String getDescripcion();
}
