/**
 * Clase principal del Banco de la ilusión.
 * @version 1.0
 * @date 1-Jul-2021
 */
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void status(Cliente c){
        for (Sujeto sujeto : c.misServicios()) {
            System.out.println("----" + sujeto.getNombre() + "----");
            sujeto.cobrar(c);
            saldoDisponible(c);
        }
    }

    public static void saldoDisponible(Cliente c){
        System.out.println("Tu saldo disponible es " + c.getSaldoDisponible());
    }

    public static void main(String[] args) {
        /* Servicios */
        Momazon momazon = new Momazon();
        Memetflix memetflix = new Memetflix();
        Issi issi = new Issi();
        Mextel mextel = new Mextel();

        /* Alicia */
        Cliente alicia = new Cliente("Alicia");
        alicia.setSaldo(20000);

        /* Bob */
        Cliente bob = new Cliente("Bob");
        bob.setSaldo(2000);

        /* Charlie */
        Cliente charlie = new Cliente("Charlie");
        charlie.setSaldo(7000);

        /* Diego */
        Cliente diego = new Cliente("Diego");
        diego.setSaldo(6000);

        /* Emma */
        Cliente emma = new Cliente("Emma");
        emma.setSaldo(9000);

        /* Enero */
        System.out.println("-------------------------------------------------");
        System.out.println("ENERO");
        System.out.println("-------------------------------------------------");

        /* Alicia */
        System.out.println("*************************************************");
        alicia.registrar(memetflix);
        memetflix.registrar(alicia, "cuatro");
        alicia.registrar(momazon);
        momazon.registrar(alicia, "");
        alicia.registrar(issi);
        issi.registrar(alicia, "100 Mbps");
        issi.cobrar(alicia);
        saldoDisponible(alicia);
        System.out.println("*************************************************\n");

        /* Bob */
        System.out.println("*************************************************");
        bob.registrar(memetflix);
        memetflix.registrar(bob, "cuatro");
        bob.registrar(momazon);
        momazon.registrar(bob, "");
        bob.registrar(mextel);
        mextel.registrar(bob, "150 Mbps");
        mextel.cobrar(bob);
        saldoDisponible(bob);
        System.out.println("*************************************************\n");

        /* Charlie */
        System.out.println("*************************************************");
        charlie.registrar(memetflix);
        memetflix.registrar(charlie, "cuatro");
        charlie.registrar(momazon);
        momazon.registrar(charlie,"");
        charlie.registrar(issi);
        issi.registrar(charlie, "50 Mbps");
        issi.cobrar(charlie);
        saldoDisponible(charlie);
        System.out.println("*************************************************\n");

        /* Diego */
        System.out.println("*************************************************");
        diego.registrar(memetflix);
        memetflix.registrar(diego,"cuatro");
        diego.registrar(issi);
        issi.registrar(diego,"50 Mbps");
        issi.cobrar(diego);
        saldoDisponible(diego);
        System.out.println("*************************************************");

        /* Emma */
        System.out.println("*************************************************");
        emma.registrar(memetflix);
        memetflix.registrar(emma,"cuatro");
        emma.registrar(momazon);
        momazon.registrar(emma,"");
        emma.registrar(issi);
        issi.registrar(emma,"100 Mbps");
        issi.cobrar(emma);
        saldoDisponible(emma);
        System.out.println("*************************************************");

        /* Febrero */
        System.out.println("-------------------------------------------------");
        System.out.println("FEBRERO");
        System.out.println("-------------------------------------------------");
        System.out.println("\n------ ALICIA ------");
        status(alicia);
        System.out.println("\n------ BOB ------");
        status(bob);
        System.out.println("\n------ CHARLIE ------");
        status(charlie);
        System.out.println("\n------ DIEGO ------");
        status(diego);
        System.out.println("\n------ EMMA ------");
        status(emma);

        /* Marzo */
        System.out.println("-------------------------------------------------");
        System.out.println("MARZO");
        System.out.println("-------------------------------------------------");
        System.out.println("\n------ ALICIA ------");
        status(alicia);
        System.out.println("\n------ BOB ------");
        status(bob);
        System.out.println("\n------ CHARLIE ------");
        status(charlie);
        System.out.println("\n------ DIEGO ------");
        status(diego);
        System.out.println("\n------ EMMA ------");
        status(emma);

        /* Abril */
        System.out.println("-------------------------------------------------");
        System.out.println("ABRIL");
        System.out.println("-------------------------------------------------");
        System.out.println("\n------ ALICIA ------");
        status(alicia);
        System.out.println("\n------ BOB ------");
        status(bob);
        System.out.println("\n------ CHARLIE ------");
        status(charlie);
        System.out.println("\n------ DIEGO ------");
        status(diego);
        System.out.println("\n------ EMMA ------");
        emma.cancelarSuscripcion(memetflix);
        memetflix.cancelar(emma);
        emma.cancelarSuscripcion(momazon);
        momazon.cancelar(emma);
        emma.cancelarSuscripcion(issi);
        issi.cancelar(emma);
        emma.registrar(mextel);
        mextel.registrar(emma,"30 Mbps");
        mextel.cobrar(emma);
        saldoDisponible(emma);

        /* Mayo */
        System.out.println("-------------------------------------------------");
        System.out.println("MAYO");
        System.out.println("-------------------------------------------------");
        System.out.println("\n------ ALICIA ------");
        status(alicia);
        System.out.println("\n------ BOB ------");
        status(bob);
        System.out.println("\n------ CHARLIE ------");
        status(charlie);
        System.out.println("\n------ DIEGO ------");
        status(diego);
        System.out.println("\n------ EMMA ------");
        status(emma);

        /* Junio */
        System.out.println("-------------------------------------------------");
        System.out.println("JUNIO");
        System.out.println("-------------------------------------------------");
        System.out.println("\n------ ALICIA ------");
        status(alicia);
        System.out.println("\n------ BOB ------");
        status(bob);
        System.out.println("\n------ CHARLIE ------");
        status(charlie);
        System.out.println("\n------ DIEGO ------");
        status(diego);
        System.out.println("\n------ EMMA ------");
        status(emma);

        /* Julio */
        System.out.println("-------------------------------------------------");
        System.out.println("JULIO");
        System.out.println("\n------ ALICIA ------");
        status(alicia);
        System.out.println("\n------ BOB ------");
        status(bob);
        System.out.println("\n------ CHARLIE ------");
        charlie.cancelarSuscripcion(issi);
        issi.cancelar(charlie);
        charlie.registrar(mextel);
        mextel.registrar(charlie,"20 Mbps");
        mextel.cobrar(charlie);
        saldoDisponible(charlie);
        memetflix.cobrar(charlie);
        saldoDisponible(charlie);
        System.out.println("\n------ DIEGO ------");
        status(diego);
        System.out.println("\n------ EMMA ------");
        status(emma);

        /* Agosto */
        System.out.println("-------------------------------------------------");
        System.out.println("AGOSTO");
        System.out.println("-------------------------------------------------");
        System.out.println("\n------ ALICIA ------");
        status(alicia);
        System.out.println("\n------ BOB ------");
        status(bob);
        System.out.println("\n------ CHARLIE ------");
        status(charlie);
        System.out.println("\n------ DIEGO ------");
        status(diego);
        System.out.println("\n------ EMMA ------");
        emma.registrar(momazon);
        momazon.registrar(emma,"");
        saldoDisponible(emma);
        mextel.cobrar(emma);
        saldoDisponible(emma);

        /* Septiembre */
        System.out.println("-------------------------------------------------");
        System.out.println("SEPTIEMBRE");
        System.out.println("-------------------------------------------------");
        System.out.println("\n------ ALICIA ------");
        status(alicia);
        System.out.println("\n------ BOB ------");
        status(bob);
        System.out.println("\n------ CHARLIE ------");
        status(charlie);
        System.out.println("\n------ DIEGO ------");
        diego.cancelarSuscripcion(memetflix);
        memetflix.cancelar(diego);
        diego.cancelarSuscripcion(issi);
        issi.cancelar(diego);
        System.out.println("\n------ EMMA ------");
        status(emma);

        /* Octubre */
        System.out.println("-------------------------------------------------");
        System.out.println("OCTUBRE");
        System.out.println("-------------------------------------------------");
        System.out.println("\n------ ALICIA ------");
        status(alicia);
        System.out.println("\n------ BOB ------");
        status(bob);
        System.out.println("\n------ CHARLIE ------");
        status(charlie);
        System.out.println("\n------ DIEGO ------");
        status(diego);
        saldoDisponible(diego);
        System.out.println("\n------ EMMA ------");
        status(emma);

        /* Noviembre */
        System.out.println("-------------------------------------------------");
        System.out.println("NOVIEMBRE");
        System.out.println("-------------------------------------------------");
        System.out.println("\n------ ALICIA ------");
        status(alicia);
        System.out.println("\n------ BOB ------");
        status(bob);
        System.out.println("\n------ CHARLIE ------");
        status(charlie);
        System.out.println("\n------ DIEGO ------");
        diego.setSaldo(6070);
        diego.registrar(issi);
        issi.registrar(diego,"30 Mbps");
        issi.cobrar(diego);
        saldoDisponible(diego);
        System.out.println("\n------ EMMA ------");
        status(emma);

        /* Diciembre */
        System.out.println("-------------------------------------------------");
        System.out.println("DICIEMBRE");
        System.out.println("-------------------------------------------------");
        System.out.println("\n------ ALICIA ------");
        status(alicia);
        System.out.println("\n------ BOB ------");
        status(bob);
        System.out.println("\n------ CHARLIE ------");
        status(charlie);
        System.out.println("\n------ DIEGO ------");
        status(diego);
        System.out.println("\n------ EMMA ------");
        status(emma);
    }
}
