/**
 * Clase para el servicio de Issi
 */

import java.util.ArrayList;
import java.util.List;

public class Issi implements Sujeto{

    private String servicio = "Issi";
    private List<Cliente> usuarioNuevo = new ArrayList<>();
    private List<Cliente> usuarioInscrito = new ArrayList<>();
    private String usuarioAct;
    private Integer aSaldo;

    /* Constructor vacío */
    public Issi(){}

    /* Regresamos el nombre del servicio */
    public String getNombre(){
        return servicio;
    }

    /* Regresamos la lista de los clientes actuales */
    public List<Cliente> getClientes(){
        return usuarioNuevo;
    }

    /* @Override registrar */
    @Override
    public void registrar(Cliente c, String usuarioAct){
        c.addSuscripcion(getNombre(), usuarioAct);
            System.out.println("¡Has contratado " + getNombre() + " " +
                               c.getNombre() + "!");
            usuarioNuevo.add(c);
    }

    /**
     * El método obtiene el nombre del servicio de un cliente
     * @return nombre del servicio del cliente.
     */
    public String getSuscripcion(Cliente c){
        return c.obtenerSuscripcion(getNombre());
    }

    /* @Override cancelar */
    @Override
    public void cancelar(Cliente c){
        System.out.println(c. getNombre() + " se ha cancelado el servicio " +
                           getNombre() + " .");
        usuarioInscrito.remove(c);
        usuarioNuevo.remove(c);
    }

    /* Override cobrar */
    @Override
    public void cobrar(Cliente c){
        aSaldo = c.getSaldoDisponible();
        String str = getSuscripcion(c);
        if(str.equals("30 Mbps")){
            if(aSaldo >= 490){
                System.out.println("$490 se te han cobrado " + c.getNombre() +
                                   " del servicio " + getNombre() + " 30Mbps");
                c.setSaldo(aSaldo-490);
            } else {
                System.out.println("No tienes suficiente dinero");
                cancelar(c);
            }
        } else if(str.equals("50 Mbps")){
            if(aSaldo >= 590){
                System.out.println("$590 se te han cobrado " + c.getNombre() +
                                   " del servicio " + getNombre() + " 50Mbps");
                c.setSaldo(aSaldo-590);
            } else {
                System.out.println("No tienes suficiente dinero");
                cancelar(c);
            }
        } else if(str.equals("100 Mbps")){
            if(aSaldo >= 780){
                System.out.println("$780 se te han cobrado " + c.getNombre() +
                                   " del servicio " + getNombre() + " 100Mbps");
                c.setSaldo(aSaldo-780);
            } else {
                System.out.println("No tienes suficiente dinero.");
                cancelar(c);
            }
        } else {
            System.out.println("Tienes que elegir una suscripción.");
        }
    }

    /* @Override actuaizar */
    public void actualizar(Cliente c, String usuarioAct){
        c.listaContratados.remove(getNombre());
        c.listaContratados.remove(usuarioAct);
        c.addSuscripcion(getNombre(), usuarioAct);
        System.out.println("Has actualizado tu suscripcion de " +
                           getNombre() + " a " + usuarioAct);
    }
}
