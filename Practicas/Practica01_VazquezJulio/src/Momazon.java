/**
 * Clase para el servicio de Momazon Prime
 */

import java.util.ArrayList;
import java.util.List;

public class Momazon implements Sujeto{

    private String servicio = "Momazon Prime Video";
    private List<Cliente> usuarioNuevo = new ArrayList<>();
    private List<Cliente> usuarioInscrito = new ArrayList<>();
    private String usuarioAct;
    private Integer aSaldo;

    /* Constructor vacío */
    public Momazon(){}

    /* @Override getNombre */
    @Override
    public String getNombre(){
        return servicio;
    }

    /* @Override getClientes */
    @Override
    public List<Cliente> getClientes(){
        return usuarioNuevo;
    }

    /* @Override registrar */
    @Override
    public void registrar(Cliente c, String usuarioAct){
        c.addSuscripcion(getNombre(), usuarioAct);
        if(usuarioInscrito.contains(c)){
            System.out.println("Bienvenido de vuelta");
            cobrar(c);
        } else {
            System.out.println("¡Has contratado " + getNombre() + " " +c.getNombre() +
                               "! El primer mes de servicio es gratuito :D");
            usuarioNuevo.add(c);
        }
    }

    /**
     * El método obtiene el nombre del servicio de un cliente
     * @return nombre del servicio del cliente.
     */
    public String getSuscripcion(Cliente c){
        return c.obtenerSuscripcion(getNombre());
    }

    /* @Override cancelar */
    @Override
    public void cancelar(Cliente c){
        System.out.println(c.getNombre() + " se ha cancelado el servicio " +
                           getNombre() + " .");
        usuarioInscrito.add(c);
        usuarioNuevo.remove(c);
    }

    /* @Override cobrar */
    @Override
    public void cobrar(Cliente c){
        aSaldo = c.getSaldoDisponible();
        if(aSaldo >= 100) {
            System.out.println("$100 se te han cobrado " + c.getNombre() +
                               " del servicio " + getNombre());
            c.setSaldo(aSaldo-100);
        } else {
            System.out.println("No tienes suficiente dinero");
            cancelar(c);
        }
    }

    /* @Override actualizar */
    @Override
    public void actualizar(Cliente c, String usuarioAct){
        c.listaContratados.remove(getNombre());
        c.listaContratados.remove(usuarioAct);
        c.addSuscripcion(getNombre(), usuarioAct);
        System.out.println("Has actualizado tu suscripcion de " +
                           getNombre() + " a " + usuarioAct);
    }
}
