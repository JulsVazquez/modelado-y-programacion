/**
 * Clase Cliente la cual modelará a los clientes que pueden contratar algún
 * servicio.
 * @version 1.0
 * @date 1-Jul-2021
 */
import java.util.ArrayList;
import java.util.List;


public class Cliente implements Observador {

    private List<Sujeto> misServicios = new ArrayList<>();
    private List<String> nombreServicio = new ArrayList<>();
    public ArrayList<String> listaContratados = new ArrayList<>();
    private String usuario;
    private String usuarioAct;
    private Sujeto sujeto;
    private Integer aSaldo;

    /* Constructor */
    public Cliente(String usuario) {
        this.usuario = usuario;
    }

    /* Override getNombre() */
    @Override
    public String getNombre(){
        return usuario;
    }

    /* @Override registrar */
    @Override
    public void registrar(Sujeto sujeto){
        this.sujeto = sujeto;
        misServicios.add(sujeto);
        nombreServicio.add(sujeto.getNombre());
    }

    /* Override nombreServicio */
    @Override
    public List<String> nombreServicio(){
        return nombreServicio;
    }

    /* Override misServicios */
    @Override
    public List<Sujeto> misServicios(){
        return misServicios;
    }

    /* Override misContrataciones */
    @Override
    public void misContrataciones(String contratacion){
        listaContratados.add(contratacion);
    }

    /* Override addSucripcion */
    @Override
    public void addSuscripcion(String sujeto, String usuarioAct){
        listaContratados.add(sujeto);
        listaContratados.add(usuarioAct);
    }

    /* @Override obtenerSuscripcion */
    @Override
    public String obtenerSuscripcion(String sujeto){
        int x = 0;
        if (listaContratados.contains(sujeto)){
            x = listaContratados.indexOf(sujeto);
            this.usuarioAct = listaContratados.get(x+1);
        }
        return usuarioAct;
    }

    /* @Override setSaldo */
    @Override
    public void setSaldo(Integer aSaldo){
        this.aSaldo = aSaldo;
    }


    /* @Override getSaldoDisponible */
    @Override
    public int getSaldoDisponible(){
        return aSaldo;
    }

    /* cancelarSuscripcion */
    public void cancelarSuscripcion(Sujeto sujeto){
        misServicios.remove(sujeto);
        listaContratados.remove(sujeto.getNombre());
        listaContratados.remove(usuarioAct);
    }

    /* @Override update */
    @Override
    public void update(){}

}
