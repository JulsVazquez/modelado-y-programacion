/**
 * Clase para el servicio de Mextel
 */

import java.util.ArrayList;
import java.util.List;

public class Mextel implements Sujeto{

    private String servicio = "Mextel";
    private List<Cliente> usuarioNuevo = new ArrayList<>();
    private List<Cliente> usuarioInscrito = new ArrayList<>();
    private String usuarioAct;
    private Integer aSaldo;

    /* Constructor vacío */
    public Mextel(){}

    /* Regresamos el nombre del servicio */
    public String getNombre(){
        return servicio;
    }

    /* Regresamos la lista de los clientes actuales */
    public List<Cliente> getClientes(){
        return usuarioNuevo;
    }

    /* @Override registrar */
    @Override
    public void registrar(Cliente c, String usuarioAct){
        c.addSuscripcion(getNombre(), usuarioAct);
            System.out.println("¡Has contratado " + getNombre() + " " +
                               c.getNombre());
            usuarioNuevo.add(c);
    }

    /**
     * El método obtiene el nombre del servicio de un cliente
     * @return nombre del servicio del cliente.
     */
    public String getSuscripcion(Cliente c){
        return c.obtenerSuscripcion(getNombre());
    }

    /* @Override cancelar */
    @Override
    public void cancelar(Cliente c){
        System.out.println(c. getNombre() + " se ha cancelado el servicio " +
                           getNombre() + " .");
        usuarioInscrito.add(c);
        usuarioNuevo.remove(c);
    }

    /* Override cobrar */
    @Override
    public void cobrar(Cliente c){
        aSaldo = c.getSaldoDisponible();
        String str = getSuscripcion(c);
        if(str.equals("20 Mbps")){
            if(aSaldo >= 349){
                System.out.println("$349 se te han cobrado " + c.getNombre() +
                                   " del servicio " + getNombre() +
                                   " 20 Mbps.");
                c.setSaldo(aSaldo-349);
            } else {
                System.out.println("No tienes suficiente dinero");
                cancelar(c);
            }
        } else if(str.equals("30 Mbps")){
            if(aSaldo >= 399){
                System.out.println("$399 se te han cobrado " + c.getNombre() +
                                   " del servicio " + getNombre() +
                                   " 30 Mbps.");
                c.setSaldo(aSaldo-399);
            } else {
                System.out.println("No tienes suficiente dinero");
                cancelar(c);
            }
        } else if(str.equals("150 Mbps")){
            if(aSaldo >= 549){
                System.out.println("$549 se te han cobrado " + c.getNombre() +
                                   " del servicio " + getNombre() +
                                   " 150 Mbps.");
                c.setSaldo(aSaldo-549);
            } else {
                System.out.println("No tienes suficiente dinero.");
                cancelar(c);
            }
        } else {
            System.out.println("Tienes que elegir una suscripción.");
        }
    }

    /* @Override actuaizar */
    public void actualizar(Cliente c, String usuarioAct){
        c.listaContratados.remove(getNombre());
        c.listaContratados.remove(usuarioAct);
        c.addSuscripcion(getNombre(), usuarioAct);
        System.out.println("Has actualizado tu suscripcion de " +
                           getNombre() + " a " + usuarioAct);
    }
}
