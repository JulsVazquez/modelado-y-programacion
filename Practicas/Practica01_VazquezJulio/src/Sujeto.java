
import java.util.ArrayList;
import java.util.List;

/**
 * Interfaz Sujeto, la cual nos permite saber qué opciones queremos
 * elegir para los diferentes servicios que da el Banco de la Ilusión
 * @version 1.0
 * @date 1-Jul-2021
 */

public interface Sujeto{

    /**
     * Nos da el nombre del servicio. El método debe de regresar el nombre del
     * servicio al que hace referencia.
     * @return nombre del servicio
     */
    public String getNombre();

    /**
     * Nos da la lista de los clientes que actualmente tienen el servicio.
     * @return lista de los clientes actuales
     */
    public List<Cliente> getClientes();

    /**
     * Registra a un cliente en la lista.
     */
    public void registrar(Cliente c, String usuarioAct);

    /**
     * Cancela a un cliente de un servicio.
     */
    public void cancelar(Cliente c);

    /**
     * Cobra a un cliente el servicio.
     */
    public void cobrar(Cliente c);

    /**
     * Actualiza el servicio de un cliente
     */
    public void actualizar(Cliente c, String usuarioAct);
}
