import java.util.List;
import java.util.ArrayList;

/**
 * Interfaz Observador
 * @version 1.0
 * @date 1-Jul-2021
 */

public interface Observador {

    /**
     * Getter del nombre del cliente
     * @return nombre del cliente
     */
    public String getNombre();

    /**
     * Registra a un Sujeto en la lista de servicios y también añade el nombre
     * del servicio a su lista.
     */
    public void registrar(Sujeto sujeto);

    /**
     * El método recupera  la lista de nombres del Servicio contratado.
     * @return nombre del Servicio.
     */
    public List<String> nombreServicio();

    /**
     * El método recupera la lista de los servicios que tiene el cliente.
     */
    public List<Sujeto> misServicios();

    /**
     * Añade a los servicios a lista de servicios contratados
     */
    public void misContrataciones(String contratacion);

    /**
     * Añade la suscripción del sujeto y también al usuario actual que lo
     * contrata.
     */
    public void addSuscripcion(String sujeto, String usuarioAct);

    /**
     * Recupera la lista de los servicios contratados y verifica si el sujeto
     * esta contenido en ella, si lo esta entonces obtiene al siguiente.
     * @return el usuario suscrito en la lista de servicios contratados
     */
    public String obtenerSuscripcion(String sujeto);

    /**
     * Asigna el saldo al cliente.
     */
    public void setSaldo(Integer aSaldo);

    /**
     * Obtiene el saldo disponible del cliente
     * @return saldo
     */
    public int getSaldoDisponible();

    /**
     * Método update() de la interfaz Observador.
     */
    public void update();
}
