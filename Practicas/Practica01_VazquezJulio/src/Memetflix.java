/**
 * Clase para el servicio de Memetflix
 */

import java.util.ArrayList;
import java.util.List;

public class Memetflix implements Sujeto{

    private String servicio = "Memetflix";
    private List<Cliente> usuarioNuevo = new ArrayList<>();
    private List<Cliente> usuarioInscrito = new ArrayList<>();
    private String usuarioAct;
    private Integer aSaldo;

    /* Constructor vacío */
    public Memetflix(){}

    /* @Override getNombre()*/
    @Override
    public String getNombre(){
        return servicio;
    }

    /* @Override getClientes() */
    @Override
    public List<Cliente> getClientes(){
        return usuarioNuevo;
    }

    /* @Override registrar */
    @Override
    public void registrar(Cliente c, String usuarioAct){
        c.addSuscripcion(getNombre(), usuarioAct);
        if(usuarioInscrito.contains(c)){
            System.out.println("Bienvenido de vuelta");
            cobrar(c);
        } else {
            System.out.println("¡Has contratado " + getNombre() + " " +c.getNombre() +
                               "! El primer mes de servicio es gratuito :D");
            usuarioNuevo.add(c);
        }
    }

    /**
     * El método obtiene el bombre del servicio de un cliente.
     * @return nombre del servicio del cliente.
     */
    public String getSuscripcion(Cliente c){
        return c.obtenerSuscripcion(getNombre());
    }

    /* @Override cancelar */
    @Override
    public void cancelar(Cliente c){
        System.out.println(c.getNombre() + " se ha cancelado el servicio " +
                           getNombre() + " .");
        usuarioInscrito.add(c);
        usuarioNuevo.remove(c);
    }

    /* @Override cobrar */
    @Override
    public void cobrar(Cliente c){
        aSaldo = c.getSaldoDisponible();
        String str = getSuscripcion(c);
        if(str.equals("uno")){
            if(aSaldo >= 100){
                System.out.println("$100 se te han cobrado " + c.getNombre() +
                                   " del servicio " + getNombre() +
                                   " un dispositivo.");
                c.setSaldo(aSaldo-100);
            } else {
                System.out.println("No tienes suficiente dinero");
                cancelar(c);
            }
        } else if(str.equals("dos")){
            if(aSaldo >= 200){
                System.out.println("$200 se te han cobrado " + c.getNombre() +
                                   " del servicio " + getNombre() +
                                   " dos dispositivos.");
                c.setSaldo(aSaldo-200);
            } else {
                System.out.println("No tienes suficiente dinero");
                cancelar(c);
            }
        } else if(str.equals("cuatro")){
            if(aSaldo >= 300){
                System.out.println("$300 se te han cobrado " + c.getNombre() +
                                   " del servicio " + getNombre() +
                                   " cuatro dispositivos.");
                c.setSaldo(aSaldo-300);
            } else {
                System.out.println("No tienes suficiente dinero.");
                cancelar(c);
            }
        } else {
            System.out.println("Tienes que elegir una suscripción.");
        }
    }

    /* @Override actualizar */
    public void actualizar(Cliente c, String usuarioAct){
        c.listaContratados.remove(getNombre());
        c.listaContratados.remove(usuarioAct);
        c.addSuscripcion(getNombre(), usuarioAct);
        System.out.println("Has actualizado tu suscripción de " +
                           getNombre() + " a " + usuarioAct);
    }
}
