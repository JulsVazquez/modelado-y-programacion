Modelado y Programacion
=======================

Práctica 1: Implementando los patrones Observer y Strategy
----------------------------------------------------------

### Fecha de entrega: martes 06 de junio, 2020

### Alumno
Julio Vázquez Alvarez - 314334398

### Compilación
En el directorio Practica01_VazquezJulio/ abrir una terminal y escribir:
```
$./practica01.sh
```

