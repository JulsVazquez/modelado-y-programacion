Modelado y Programación Ayudantía del 19/Jul/2021
=============================================

Builder y Prototype
------------------

### Builder
El patrón permite construir distintas representaciones de objetos complejos 
usando constructores de objetos simples.

La construcción de los objetos complejos dependerá de un constructor o director
que tratará a cada componente como una etapa de la construcción. Se pueden tener
directores concretos que se dediquen a un objeto específico, o directores 
abstractos, que permitirán la construcción de cualquier combinación de 
componentes.

Cada componente está modularizado.

### Prototype

Este patrón permite crear objetos nuevos a partir de instancias existentes. El 
cliente puede instanciar objetos sin conocer la clase específica de la que se va
a instanciar. El proceso de clonación se delega al objeto que está siendo 
clonado.

Se utiliza cuando la creación de objetos es muy costosa. El beneficio está en 
tener en caché los objetos que se usarán y clonarlos en ejecución para su uso.
