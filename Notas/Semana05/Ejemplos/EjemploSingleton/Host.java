public class Host{

	private static Host host;

	private Empleado empleadoBono;

	private Host(Empleado e){
		empleadoBono = e;
	}

	/**
	* Metodo que provee el punto de acceso al singleton.
	* Este metodo debe ser sincronizado en caso de que 
	* dos o mas hilos accedan al metodo a la vez
	*/
	public synchronized static Host obtenerHost(Empleado e){
		if(host == null)
			host = new Host(e);
		return host;
	}

	public void infoSesion(){
		if (host == null)
			System.out.println("Aun no ha iniciado sesion ningun empleado.");
		System.out.println("El empleado ganador del bono es: " + 
							empleadoBono.getUsuario()
							+ " con numero de identificacion: " + 
							empleadoBono.getIdEmpleado());
	}
}