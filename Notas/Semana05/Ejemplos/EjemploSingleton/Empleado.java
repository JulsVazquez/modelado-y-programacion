public class Empleado extends Thread{
	private String usuario;
	private int idEmpleado;

	public Empleado(String usuario, int idEmpleado){
		this.usuario = usuario;
		this.idEmpleado = idEmpleado;
	}

	public String getUsuario(){
		return usuario;
	}

	public int getIdEmpleado(){
		return idEmpleado;
	}

	@Override
	public void run(){
		Host nuevaSesion = Host.obtenerHost(this);
		nuevaSesion.infoSesion();
	}

}