public abstract class Hamburguesa implements Componente{

	private Empaque empaque = new Caja();

	@Override
	public Empaque getEmpaque(){
		return empaque;
	}

	@Override
	public abstract double precio();

}