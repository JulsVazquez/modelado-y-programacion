public class DemoBuilder{
	public static void main(String[] args) {
   
      Builder builder = new Builder();

      Comida combo1 = builder.preparaComboUno();
      System.out.println("Comida Combo 1");
      combo1.muestraComida();
      System.out.println("Total: " + combo1.obtenCosto());

      Comida combo2 = builder.preparaComboDos();
      System.out.println("\n\nComida doble");
      combo2.muestraComida();
      System.out.println("Total: " + combo2.obtenCosto());

      Comida aElegir = builder.preparaComida("tocino", "cocacola", "curly");
      System.out.println("\n\nComida preparada");
      aElegir.muestraComida();
      System.out.println("Total: " + aElegir.obtenCosto());
   }
}