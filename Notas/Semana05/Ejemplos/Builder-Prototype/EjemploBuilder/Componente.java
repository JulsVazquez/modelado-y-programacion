public interface Componente{
	public String nombre();
	public Empaque getEmpaque();
	public double precio();
}