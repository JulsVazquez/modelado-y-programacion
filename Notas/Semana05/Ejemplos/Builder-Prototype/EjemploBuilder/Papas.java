public abstract class Papas implements Componente{

	private Empaque empaque = new Bolsa();

	@Override
	public Empaque getEmpaque(){
		return empaque;
	}

	@Override
	public abstract double precio();

}