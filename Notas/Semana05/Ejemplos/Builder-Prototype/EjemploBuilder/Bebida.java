public abstract class Bebida implements Componente{

	private Empaque empaque = new Vaso();

	@Override
	public Empaque getEmpaque(){
		return empaque;
	}

	@Override
	public abstract double precio();

}