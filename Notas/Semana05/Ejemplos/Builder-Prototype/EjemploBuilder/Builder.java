public class Builder {

   /**
   * Metodo builder especifico para el combo uno. Consiste en
   * una hamburguesa Vegetariana, Coca cola y papas a la Francesa.
   */
   public Comida preparaComboUno(){
      Comida comida = new Comida();
      comida.agregaComponente(new Vegetariana());
      comida.agregaComponente(new CocaCola());
      comida.agregaComponente(new Francesa());
      return comida;
   }   

   /**
   * Metodo builder especifico para el combo dos. Consiste en
   * una hamburguesa Con Tocino, Sprite y papas Curly.
   */
   public Comida preparaComboDos(){
      Comida comida = new Comida();
      comida.agregaComponente(new Tocino());
      comida.agregaComponente(new Sprite());
      comida.agregaComponente(new Curly());
      return comida;
   }

   /**
   * Metodo builder general. Puede crear cualquier combinacion 
   * para una comida.
   * Se reciben objetos como parametros. 
   * La creacion se delega a los constructores.
   * @param ham La hamburguesa elegida.
   * @param bebida El refresco elegido.
   * @param papas Las papas elegidas.
   */
   public Comida preparaComida(String ham, String bebida, String papas){
      /* Este proceso se hace por pasos. En cada paso se agrega un nuevo
      Componente de la comida. */
      Comida comida = new Comida();

      //La primera etapa es para la hamburguesa.
      if(ham.equalsIgnoreCase("vegetariana")){
         comida.agregaComponente(new Vegetariana());
      } else if(ham.equalsIgnoreCase("tocino")){
         comida.agregaComponente(new Tocino());
      }

      //La segunda etapa es para el refresco.
      if(bebida.equalsIgnoreCase("cocacola")){
         comida.agregaComponente(new CocaCola());
      } else if(bebida.equalsIgnoreCase("sprite")){
         comida.agregaComponente(new Sprite());
      }

      //La tercera etapa es para las papas.
      if(papas.equalsIgnoreCase("francesa")){
         comida.agregaComponente(new Francesa());
      } else if(papas.equalsIgnoreCase("curly")){
         comida.agregaComponente(new Curly());
      }

      //Se regresa la comida construida con los componentes elegidos.
      return comida;
   }
}