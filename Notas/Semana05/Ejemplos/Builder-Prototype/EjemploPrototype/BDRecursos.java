public class BDRecursos{
	
	private Recurso[] elementos;

	public BDRecursos(){
		elementos = new Recurso[]{new Recurso("Roca", 2, 1), 
									new Recurso("Carbon", 3, 2), 
									new Recurso("Plata", 2, 5), 
									new Recurso("Oro", 2, 10),
									new Recurso("Diamante", 1, 50)};
	}


	public Recurso getRecurso(int posicion){
		int i = posicion%elementos.length;
		return (Recurso)elementos[i].clone();
	}

	public void imprimirBD(){
		System.out.println("*****Lista de recursos*****");
		for(int i = 0; i < elementos.length; i++){
			System.out.println(elementos[i].mostrarInfo());
		}
	}
}