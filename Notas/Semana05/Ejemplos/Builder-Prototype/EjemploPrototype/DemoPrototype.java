import java.util.Random;

public class DemoPrototype{

	private Elem[] nivel;
	public BDEnemigos enemigos = new BDEnemigos();
	public BDRecursos recursos = new BDRecursos();

	private void generarNivel(int tam){
		Generador generador = new Generador();

		Random r = new Random();

		nivel = new Elem[tam];
		for(int i = 0; i < nivel.length; i++){
			int azar = r.nextInt(5);
			if(azar <= 2){
				Enemigo nEnemigo = generador.getEnemigoCueva(enemigos, r.nextInt(2)+1);
				nivel[i] = nEnemigo;
			} else{
				Recurso nRecurso = generador.getRecursoCueva(recursos, r.nextInt(2)+1);
				nivel[i] = nRecurso;
			}
		}

		System.out.println("\nLos elementos en este nivel son los siguientes:");
		for(int i = 0; i < nivel.length; i++){
			System.out.println(nivel[i].mostrarInfo());
		}

	}
	public static void main(String[] args){
		DemoPrototype proto = new DemoPrototype();

		proto.enemigos.imprimirBD();
		proto.recursos.imprimirBD();
		proto.generarNivel(10);
	}
}