public class DemoAbstractFactory{
	public static void main(String[] args){

		//Fabrica productora.
		FactoryProducer productora = new FactoryProducer();

		//Fabricas para cada parte del auto.
		AbstractFactory fabricaLLantas = 
							productora.getFactory("LLANTA");
		AbstractFactory fabricaCarrocerias = 
							productora.getFactory("CARROCERIA");
		AbstractFactory fabricaMotores = 
							productora.getFactory("MOTOR");

		//Se crean las llantas.
		LLanta llantasNuevas = 
					(LLanta)fabricaLLantas.getComponente("monstruo");
		llantasNuevas.crearLLanta();

		LLanta llantasNuevas2 = 
					(LLanta)fabricaLLantas.getComponente("deportiva");
		llantasNuevas2.crearLLanta();

		//Se crean las carrocerias.
		Carroceria carroceriaNueva = 
					(Carroceria)fabricaCarrocerias.getComponente("deportiva");
		carroceriaNueva.crearCarroceria();

		Carroceria carroceriaNueva2 = 
					(Carroceria)fabricaCarrocerias.getComponente("deportiva");
		carroceriaNueva2.crearCarroceria();

		//Se crean los motores.
		Motor motorNuevo = 
					(Motor)fabricaMotores.getComponente("monstruo");
		motorNuevo.crearMotor();

		Motor motorNuevo2 = 
					(Motor)fabricaMotores.getComponente("deportivo");
		motorNuevo2.crearMotor();

		//Se crea el auto nuevo con esas partes.
		Auto autoNuevo = 
					new Auto(llantasNuevas, carroceriaNueva, motorNuevo);
		autoNuevo.muestraAuto();

		System.out.println("\n");

		Auto autoNuevo2 = 
					new Auto(llantasNuevas2, carroceriaNueva2, motorNuevo2);
		autoNuevo2.muestraAuto();

	}
}