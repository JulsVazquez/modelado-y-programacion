public class Mago{
	
	public void conjurar(){
		System.out.println("El mago conjura una bola de fuego.");
	}

	public void proteger(){
		System.out.println("El mago se defiende con un campo de fuerza.");
	}

	public void abrirPortal(){
		System.out.println("El mago abre un portal y se va.");
	}
}