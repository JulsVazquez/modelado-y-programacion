import java.util.Scanner;

public class DemoFacade{

	public static void main(String[] args){

		EscuadronFacade escuadron = new EscuadronFacade();
		Scanner sc = new Scanner(System.in);
		int opcion = 0;

		do{
			System.out.println("Ingrese la opcion que deseas " + 
				"que tu escuadron realice:\n\n" + 
				"1.- Atacar\n" +
				"2.- Defender\n" + 
				"3.- Escapar\n" + 
				"0.- Salir\n");
			
			boolean esNumero = false;
			while(!esNumero){
				String opcionUsuario = sc.nextLine();
				try{
					opcion = Integer.parseInt(opcionUsuario);
					esNumero = true;
				} catch(NumberFormatException ex){
					//ex.printStackTrace();
					System.out.println("Elige una opcion correcta.\n\n" + 
						"1.- Atacar\n" +
						"2.- Defender\n" + 
						"3.- Escapar\n" + 
						"0.- Salir\n");
				}
			}

			switch(opcion){
					case 1:
						System.out.println("\n----------Ataquen----------");
						escuadron.lanzarAtaque();
						System.out.println("\n");
						break;

					case 2:
						System.out.println("\n----------Defiendan----------");
						escuadron.defenderPosiciones();
						System.out.println("\n");
						break;

					case 3:
						System.out.println("\n----------Escapen----------");
						escuadron.retirada();
						System.out.println("\n");
						break;

					case 0:
						System.out.println("Adios.");
						break;

					default:
						System.out.println("Elige una opcion valida.\n");
						break;

				}

		}while (opcion != 0);
	}
}