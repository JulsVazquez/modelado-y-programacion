public class EscuadronFacade{

	private Caballero caballero;
	private Arquero arquero;
	private Mago mago;

	public EscuadronFacade(){
		caballero = new Caballero();
		arquero = new Arquero();
		mago = new Mago();
	}

	public void lanzarAtaque(){
		caballero.atacar();
		arquero.disparar();
		mago.conjurar();
	}

	public void defenderPosiciones(){
		caballero.defender();
		arquero.cubrirse();
		mago.proteger();
	}

	public void retirada(){
		caballero.escapar();
		arquero.correr();
		mago.abrirPortal();
	}
}