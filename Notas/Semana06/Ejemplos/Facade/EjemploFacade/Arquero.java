public class Arquero{
	
	public void disparar(){
		System.out.println("El arquero lanza una flecha.");
	}

	public void cubrirse(){
		System.out.println("El arquero se cubre tras un arbol.");
	}

	public void correr(){
		System.out.println("El arquero se va corriendo.");
	}
}