public class Caballero{

	public void atacar(){
		System.out.println("El caballero ataca con espada.");
	}

	public void defender(){
		System.out.println("El caballero se defiende con escudo.");
	}

	public void escapar(){
		System.out.println("El caballero se va corriendo.");
	}
	
}