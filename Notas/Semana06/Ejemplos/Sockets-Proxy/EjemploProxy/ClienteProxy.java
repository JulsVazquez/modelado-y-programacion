import java.io.Serializable;

public class ClienteProxy implements Serializable, InterfazCliente{
	private Cliente clienteReal;
	private String recurso;

	public ClienteProxy(Cliente clienteReal){
		this.clienteReal = clienteReal;
	}

	public void reportar(){
		clienteReal.reportar();
	}

	public void actualizarDatos(String nRecurso){
		recurso = nRecurso;
	}

	public void actualizarEnReales(){
		clienteReal.actualizarDatos(recurso);
	}
}