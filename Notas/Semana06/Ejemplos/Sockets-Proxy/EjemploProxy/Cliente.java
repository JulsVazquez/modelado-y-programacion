import java.io.Serializable;

public class Cliente implements Serializable, InterfazCliente{

	private String rfc;
	private String nombre;

	private String recurso = "No hay recursos aun :(";

	public Cliente(String rfc, String nombre){
		this.rfc = rfc;
		this.nombre = nombre;
	}

	public void reportar() {
		System.out.println("Cliente{" + "rfc=" + rfc + ", nombre=" + nombre
							+ ", recursos= " + recurso + " }");
    }

    public void actualizarDatos(String nRecurso){
    	//simulando un sistema seguro.
    	System.out.println("Verificando datos...");
    	recurso = nRecurso;
    }
}