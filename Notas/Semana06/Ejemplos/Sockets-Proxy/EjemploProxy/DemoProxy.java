import java.net.*;
import java.io.*;

public class DemoProxy{
	
	public static void main(String[] args) {
        /*Al correr el programa escribe "server" o "client".
          java DemoProxy server en una terminal
          java DemoProxy client en otra,*/
        
        if ("server".equals(args[0])) {
            startServer();
        } else if("client".equals(args[0])){
            startClient();
        }
    }

    public static void startServer() {
        String recurso = "Este es el recurso.";
        try{
            ServerSocket server = new ServerSocket(8080);
            while(true){
                Socket s = server.accept();
                RemoteMessagePassing rmp = new RemoteMessagePassing(s);
                ClienteProxy proxy = (ClienteProxy)rmp.receive();
                System.out.println("Reporte del cliente para el servidor.");
                proxy.reportar();
                System.out.println("Enviando recursos al cliente...");
                rmp.send(recurso);
                System.out.println("Envio exitoso.");
                rmp.close();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void startClient() {
        try{
            Socket s = new Socket("localhost", 8080);
            RemoteMessagePassing rmp = new RemoteMessagePassing(s);
            Cliente clienteAEnviar = new Cliente("HEFF8812131Q7", "Fulano");
            ClienteProxy proxy = new ClienteProxy(clienteAEnviar);
            clienteAEnviar.reportar();
            rmp.send(proxy);
            String recurso = (String)rmp.receive();
            proxy.actualizarDatos(recurso);
            proxy.actualizarEnReales();
            clienteAEnviar.reportar();
            rmp.close();
        }catch (UnknownHostException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}