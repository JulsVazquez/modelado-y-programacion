Ayudantía de Modelado y Programación 05/Julio/2021
==================================================

Template y State
----------------

### Template 

Se define el esqueleto de un algoritmo en un método, delegando algunos pasos a
ciertas subclses. Permite que las usbclases redefinan ciertos pasos de un 
algoritmo sin cambiar su estructura. Se compone de métodos fijos (que se hacen
siempre) y métodos delegados (que dependen de cada subclase).

Debemos plantear una tarea y los pasos a elaborar y de ahí determinar los 
esenciales y cuáles varían.

Creamos clases abstractas porque definimos algunos métodos que existen desde 
antes, pero delegamos otros a clases que heredan de esa clase padre.

Método Teplate es el que recopila todas las acciones del algoritmo.

Nos permite tener escalabilidad del sistema.

### State

Este patrón permite a un objto de contexto cambiar su comportamiento cuando su 
estado interno cambia. El objeto de contexto cambia su estado a partir de 
transiciones diseñadas por el programador en un diagrama de estados. Cuando el
obketo de contexto cambie su estado (por el sistema o por el usuario), dara la 
impresión de haber cambiado de clase.

Tenemos que pensar cuando pasa de una transición a otra.

Un diagrama de estado es una gráfica simple que permite búcles.

La pre y post condiciones se escriben en la documentación de nuestro sistema.

Los óvalos son las clases.

Las flechas generalmente representan métodos.

Considerar cuál es el objeto que debe cambiar su estado interno y definir una 
familia de estados los cuales deben respetar lo que esta definido en su propia
interfaz, las transiciones que quiero que haga a partir del diagrama de estados.

El objeto de contexto funciona sin que el usuario se entere de que pasó por 
detrás.

Aparentamos hacer que un objeto sean varios objetos.
