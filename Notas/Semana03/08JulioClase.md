Notas de la clase de Modelado y Programación 08/Julio/2021
==========================================================

Decorator/Adapter
-----------------

### Decorator
### Principio de diseño:
_Las clases deben estar abiertas a la extensión, pero cerradas a la
modificación_

Objetivo: Permitir que las ...

El patron decorador atribuye responsabilidades adicionales a un objeto 
dinámicamente.

Los decoradores proporcionan una alternativa flexible a las subclases para 
ampliar la funcionalidad.

Tenemos un diseño resistente al cambio y lo suficientemente flexible para
soportar cambios en los requerimientos.

Las clases extienden ...

Cada componente se puede usar solo o envuelto por un decorador.

Cada decorador envuelve un componente, lo que significa ...

Adjuntar responsabilidades adicionales a un objeto de forma dinámica

Decorator proporciona una alternativa flexible a las subclases para ampliar la 
funcionalidad

Podría pasar que el cliente tenga peticiones extrañas.

### Adapter

Creamos unas clase que se adpate a la nueva interfaz e implemente la clase que 
espera el código.

El patron convierte la interesa de una clase en otra interfaz que se adapte a la 
que el cliente necesite.
