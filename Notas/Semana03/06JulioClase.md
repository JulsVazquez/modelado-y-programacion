Clase de Modelado y Programación 06/Julio/2021
==============================================

Iterator/Composite
------------------

El patron Composite nos permite construir estructuras de objetos en forma de
árboles que contineen tanto composiciones de objetos como objetos individuales
como nodos.

Al usar una estructura compuesta, podemos aplicar las mismas operaciones sobre 
los compuestos y los objetos individuales. En otras palabras, en la mayorñia de 
los casos podemos ignorar las diferencias entre composiciones de objetos y 
objetos individuales.
