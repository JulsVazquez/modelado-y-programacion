Ayudantía de Modelado y Programación 07/Julio/2020
==================================================

Composite e Iterator
--------------------

Son patrones especificos y que se utilizan en casos muy particulares, pero al 
usarlos son muy potentes.

### Composite
Pensar en árboles.
Este patrón permite componer objetos en una estructura tipo árbol para 
representar jerarquías. Así el usuario puede tratar uniformemente objetos 
individuales o compuestos.

Al trabajar con composite debemos de pensar de manera recursiva.

Debemos establecer una relación jerarquíca al usar Composite.

### Iterator
El patrón iterator proporciona una forma de acceder a los elementos 
secuencialmente de una estructura de objetos de agregación sin exponer su 
representación básica.

El uso de Iterator permitee leer las estructuras de datos, manejarlas, pero no 
darle la información al cliente.

Para saber si una estructura tiene iterador debemos revisar Collection en la API
de Java

El rombo negro es una clase anidada

Iterador nos hace leer estructuras de datos que queremos proteger.
