public interface EstadoCajero{
	void insertarTarjeta();

	void expulsarTarjeta();

	void ingresarPIN(int pinIngresado);

	void hacerRetiro(int efectivoARetirar);
}