public class Cajero{
	
	EstadoCajero conTarjeta;
	EstadoCajero sinTarjeta;
	EstadoCajero pinCorrecto;
	EstadoCajero cajeroSinDinero;

	EstadoCajero estadoActual;

	int dineroEnCajero = 2000;
	boolean pinCorrectoIngresado = false;

	public Cajero(){
		conTarjeta = new ConTarjeta(this);
		sinTarjeta = new SinTarjeta(this);
		pinCorrecto = new PinCorrecto(this);
		cajeroSinDinero = new CajeroSinDinero(this);

		estadoActual = sinTarjeta;

		if(dineroEnCajero < 0)
			estadoActual = cajeroSinDinero;
	}

	public int getDineroEnCajero(){
		return dineroEnCajero;
	}

	public void setPinCorrectoIngresado(boolean valor){
		pinCorrectoIngresado = valor;
	}

	public void setDineroEnCajero(int nuevoMonto){
		dineroEnCajero = nuevoMonto;
	}

	public void asignarEstadoCajero(EstadoCajero nuevoEstado){
		estadoActual = nuevoEstado;
	}

	public void insertarTarjeta(){
		estadoActual.insertarTarjeta();
	}

	public void expulsarTarjeta(){
		estadoActual.expulsarTarjeta();
	}

	public void ingresarPIN(int pinIngresado){
		estadoActual.ingresarPIN(pinIngresado);
	}

	public void hacerRetiro(int efectivoARetirar){
		estadoActual.hacerRetiro(efectivoARetirar);
	}

	public EstadoCajero getEstadoConTarjeta(){
		return conTarjeta;
	}

	public EstadoCajero getEstadoSinTarjeta(){
		return sinTarjeta;
	}

	public EstadoCajero getEstadoPinCorrecto(){
		return pinCorrecto;
	}

	public EstadoCajero getEstadoCajeroSinDinero(){
		return cajeroSinDinero;
	}

}