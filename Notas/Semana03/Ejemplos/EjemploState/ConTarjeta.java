public class ConTarjeta implements EstadoCajero{
	
	Cajero cajero;

	public ConTarjeta(Cajero nuevoCajero){
		cajero = nuevoCajero;
	}

	public void insertarTarjeta(){
		System.out.println("Insertar tarjeta. Solo se puede ingresar una tarjeta a la vez.");
	}

	public void expulsarTarjeta(){
		System.out.println("Expulsar tarjeta. Tu tarjeta ha sido expulsada.");
		cajero.asignarEstadoCajero(cajero.getEstadoSinTarjeta());
	}

	public void ingresarPIN(int pinIngresado){
		System.out.println("Por favor ingresa tu PIN.");
		if(pinIngresado == 1234){
			System.out.println("Has ingresado el PIN correcto.");
			cajero.setPinCorrectoIngresado(true);
			cajero.asignarEstadoCajero(cajero.getEstadoPinCorrecto());
		} else {
			System.out.println("Has ingresado el PIN incorrecto.");
			cajero.setPinCorrectoIngresado(false);
			System.out.println("Tu tarjeta ha sido expulsada.");
			cajero.asignarEstadoCajero(cajero.getEstadoSinTarjeta());
		}
	}

	public void hacerRetiro(int efectivoARetirar){
		System.out.println("Hacer retiro. No se ha ingresado el PIN.");
	}
}