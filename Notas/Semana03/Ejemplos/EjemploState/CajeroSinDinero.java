public class CajeroSinDinero implements EstadoCajero{
	
	Cajero cajero;

	public CajeroSinDinero(Cajero nuevoCajero){
		cajero = nuevoCajero;
	}

	public void insertarTarjeta(){
		System.out.println("Insertar tarjeta. El cajero no tiene dinero." +
							"\nTu tarjeta ha sido expulsada.");
	}

	public void expulsarTarjeta(){
		System.out.println("Expulsar tarjeta. El cajero no tiene dinero." +
							"\nNo hay tarjeta a expulsar.");
	}

	public void ingresarPIN(int pinIngresado){
		System.out.println("Ingresar PIN. El cajero no tiene dinero.");
	}

	public void hacerRetiro(int efectivoARetirar){
		System.out.println("Hacer retiro. El cajero no tiene dinero.");	
	}
}