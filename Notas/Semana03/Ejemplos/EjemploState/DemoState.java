public class DemoState{
	public static void main(String[] args){
		Cajero cajero = new Cajero();

		System.out.println("Caso 1: Ingresar y expulsar tarjeta.");
		cajero.insertarTarjeta();
		cajero.expulsarTarjeta();
		System.out.println("\n");

		System.out.println("Caso 2: Ingresar tarjeta con PIN incorrecto.");
		cajero.insertarTarjeta();
		cajero.ingresarPIN(1111);
		System.out.println("\n");

		System.out.println("Caso 3: Ingresar tarjeta con PIN correcto y retirar 200");
		cajero.insertarTarjeta();
		cajero.ingresarPIN(1234);
		cajero.hacerRetiro(200);
		System.out.println("\n");

		System.out.println("Caso 4: Ingresar tarjeta con PIN correcto y no se puede retirar 2000.");
		cajero.insertarTarjeta();
		cajero.ingresarPIN(1234);
		cajero.hacerRetiro(2000);
		System.out.println("\n");

		System.out.println("Caso 5: Ingresar tarjeta con PIN correcto y se retiran 1800.");
		cajero.insertarTarjeta();
		cajero.ingresarPIN(1234);
		cajero.hacerRetiro(1800);
		System.out.println("\n");

		System.out.println("Caso 6: Ingresar tarjeta con PIN correcto pero el cajero no tiene dinero.");
		cajero.insertarTarjeta();
		cajero.ingresarPIN(1234);
		System.out.println("\n");

	}
}