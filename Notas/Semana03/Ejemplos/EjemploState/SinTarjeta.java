public class SinTarjeta implements EstadoCajero{
	
	Cajero cajero;

	public SinTarjeta(Cajero nuevoCajero){
		cajero = nuevoCajero;
	}

	public void insertarTarjeta(){
		System.out.println("Se ha insertado una tarjeta.");
		cajero.asignarEstadoCajero(cajero.getEstadoConTarjeta());
	}

	public void expulsarTarjeta(){
		System.out.println("Expulsar tarjeta.No has ingresado una tarjeta.");
	}

	public void ingresarPIN(int pinIngresado){
		System.out.println("Ingresar PIN. No has ingresado una tarjeta.");
	}

	public void hacerRetiro(int efectivoARetirar){
		System.out.println("Hacer retiro. No has ingresado una tarjeta.");
	}
	
}