public class PinCorrecto implements EstadoCajero{
	
	Cajero cajero;

	public PinCorrecto(Cajero nuevoCajero){
		cajero = nuevoCajero;
	}

	public void insertarTarjeta(){
		System.out.println("Insertar tarjeta. Ya ingresaste una tarjeta.");
	}

	public void expulsarTarjeta(){
		System.out.println("Expulsar tarjeta. Tu tarjeta ha sido expulsada.");
		cajero.asignarEstadoCajero(cajero.getEstadoSinTarjeta());
	}

	public void ingresarPIN(int pinIngresado){
		System.out.println("Ingresar PIN. Ya has ingresado tu PIN.");
	}

	public void hacerRetiro(int efectivoARetirar){
		if(efectivoARetirar > cajero.getDineroEnCajero()){
			System.out.println("Hacer retiro. No tienes suficiente dinero.");
			System.out.println("Tu tarjeta ha sido expulsada.");
			cajero.asignarEstadoCajero(cajero.getEstadoSinTarjeta());
		} else {
			System.out.println("Hacer retiro. Se ha retirado " + efectivoARetirar);
			cajero.setDineroEnCajero(cajero.getDineroEnCajero() - efectivoARetirar);

			System.out.println("Tu tarjeta ha sido expulsada.");
			cajero.asignarEstadoCajero(cajero.getEstadoSinTarjeta());

			if(cajero.getDineroEnCajero() <= 0)
				cajero.asignarEstadoCajero(cajero.getEstadoCajeroSinDinero());
		}
	}
	
}