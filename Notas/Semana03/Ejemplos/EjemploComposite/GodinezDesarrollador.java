public class GodinezDesarrollador implements Godinez{
	public long id_godinez;
	public String nombre;

	public GodinezDesarrollador(long id, String nombre){
		this.id_godinez = id;
		this.nombre = nombre;
	}

	@Override
	public void getDatosEmpleado(){
		System.out.println("   id: " + id_godinez + " nombre: " + nombre 
			+ " puesto: Godinez Desarrollador");

		System.out.println("      No tiene subordinados explotables TT_TT\n");
	}
}