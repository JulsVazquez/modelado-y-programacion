import java.util.LinkedList;

public class GodinezManager implements Godinez{
	public long id_godinez;
	public String nombre;
	public LinkedList<GodinezDesarrollador> godinezDesarrolladorExplotables = new LinkedList<GodinezDesarrollador>();

	public GodinezManager(long id, String nombre){
		this.id_godinez = id;
		this.nombre = nombre;
	}

	@Override
	public void getDatosEmpleado(){
		System.out.println("id: " + id_godinez + " nombre: " + nombre 
			+ " puesto: Godinez Manager");

		System.out.println("   Subordinados explotables del Manager: \n");

		for(Godinez godi:godinezDesarrolladorExplotables){
			System.out.print("   ");
        	godi.getDatosEmpleado();
		}
	}

	public void contratarGodinez(GodinezDesarrollador godi){
		godinezDesarrolladorExplotables.add(godi);
	}

	public void despedirGodinez(GodinezDesarrollador godi){
		godinezDesarrolladorExplotables.remove(godi);
	}
}