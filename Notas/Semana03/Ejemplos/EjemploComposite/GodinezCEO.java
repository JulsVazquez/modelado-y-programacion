import java.util.LinkedList;

public class GodinezCEO implements Godinez{
	public long id_godinez;
	public String nombre;
	public LinkedList<GodinezManager> godinezManagerExplotables = new LinkedList<GodinezManager>();

	public GodinezCEO(long id, String nombre){
		this.id_godinez = id;
		this.nombre = nombre;
	}

	@Override
	public void getDatosEmpleado(){
		System.out.println("id: " + id_godinez + " nombre: " + nombre 
			+ " puesto: Godinez CEO");
		
		System.out.println("Subordinados explotables del CEO: \n");

		for(Godinez godi:godinezManagerExplotables){
			System.out.print("   ");
        	godi.getDatosEmpleado();
		}
	}

	public void contratarGodinez(GodinezManager godi){
		godinezManagerExplotables.add(godi);
	}

	public void despedirGodinez(GodinezManager godi){
		godinezManagerExplotables.remove(godi);
	}
}