public class DemoComposite{
	
	public static void main(String[] args){
		//Crear a los empleados.
		GodinezCEO ceo = new GodinezCEO(10001, "GUILLERMO VELARDE CALLEJAS");

		GodinezManager man1 = new GodinezManager(20001, "FRANCISCO ARTOLA OLMOS");
		GodinezManager man2 = new GodinezManager(20002, "GLORIA ROSELLO BASTIDA");
		GodinezManager man3 = new GodinezManager(20003, "SILVIA DE LEON DE ANDRES");

		GodinezDesarrollador des1 = new GodinezDesarrollador(30001, "MARIA LUISA VICO ANDREU");
		GodinezDesarrollador des2 = new GodinezDesarrollador(30002, "RICARDO ROCAMORA REQUEJO");
		GodinezDesarrollador des3 = new GodinezDesarrollador(30003, "SALVADOR PEREIRA URIARTE");
		GodinezDesarrollador des4 = new GodinezDesarrollador(30004, "MARIA MAGDALENO MESTRES");
		GodinezDesarrollador des5 = new GodinezDesarrollador(30005, "PAULA SAMPER ROMO");
		GodinezDesarrollador des6 = new GodinezDesarrollador(30006, "JUAN MANUEL MESAS MATUTE");
		GodinezDesarrollador des7 = new GodinezDesarrollador(30007, "TOMAS SASTRE ESPINO");

		//El CEO contrata a los managers.
		ceo.contratarGodinez(man1);
		ceo.contratarGodinez(man2);
		ceo.contratarGodinez(man3);

		//Los managers contratan a los desarrolladores.
		man1.contratarGodinez(des1);
		man1.contratarGodinez(des2);

		man2.contratarGodinez(des3);
		man2.contratarGodinez(des4);

		man3.contratarGodinez(des5);
		man3.contratarGodinez(des6);
		man3.contratarGodinez(des7);

		//Se imprime el organigrama general.
		System.out.println("\nOrganigrama Godinez inc.\n");
		ceo.getDatosEmpleado();

		//Se imprime el organigrama de un departamento.
		System.out.println("\nOrganigrama Departamento de Manager 1\n");
		man1.getDatosEmpleado();
		

	}
}