public class Computologo extends Estudiante{
	@Override
	void estudiar(){
		System.out.println("Estudiar (TT_TT). Pico teclas " +
			"todo el dia y le grito a mi compu.");

		if (Math.random() < 0.7)
			tieneTarea = true;
	}

	@Override
	void hacerTarea(){
		if(tieneTarea == true)
			System.out.println("Hacer tarea (TT_TT). Le sigo " +
				"gritando a mi compu.");
	}
}