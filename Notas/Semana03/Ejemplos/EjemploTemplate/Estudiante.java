public abstract class Estudiante{
	
	public boolean tieneTarea = false;

	/*
	* Metodo abstracto para que cada clase lo modifique a su antojo.
	*/
	abstract void estudiar();

	abstract void hacerTarea();

	/*
	* Metodo template (plantilla)
	*/
	public final void rutinaDiaria(){
		despertar();
		desayunar();
		viajarAFacultad();
		estudiar();
		comer();
		regresarACasa();
		hacerTarea();
		cenar();
		dormir();
	}

	void despertar(){
		System.out.println("Despertar (TT_TT). Buenos dias.");
	}

	void desayunar(){
		System.out.println("Desayunar (TT_TT). Estoy desayunando.");
	}

	void viajarAFacultad(){
		System.out.println("Viajar a la facultad (TT_TT). Maldito metro...");
	}

	void comer(){
		System.out.println("Comer (TT_TT). Estoy comiendo.");
	}

	void regresarACasa(){
		System.out.println("Viajar a casa (TT_TT). Maldito metro otra vez....-___-");
	}

	void cenar(){
		System.out.println("Cenar (TT_TT). Estoy cenando.");
	}

	void dormir(){
		System.out.println("Dormir (TT_TT). Zzzzzzz...");
	}
}