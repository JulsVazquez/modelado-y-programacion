public class Fisico extends Estudiante{
	@Override
	void estudiar(){
		System.out.println("Estudiar (TT_TT). Imaginemos una " +
			"vaca esferica...");

		if (Math.random() < 0.7)
			tieneTarea = true;
	}

	@Override
	void hacerTarea(){
		if(tieneTarea == true)
			System.out.println("Hacer tarea (TT_TT). Hago " +
				"demostraciones con las vacas esfericas" +
				" de la clase de hoy.");
	}
}