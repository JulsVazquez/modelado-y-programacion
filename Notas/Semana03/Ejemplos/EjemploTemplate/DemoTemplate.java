public class DemoTemplate{
	
	public static void main(String[] args){

		System.out.println("Rutina diaria de un Terrologo:\n");
		Estudiante est = new Terrologo();
		est.rutinaDiaria();
		System.out.println("\n");

		System.out.println("Rutina diaria de un Computologo:\n");
		est = new Computologo();
		est.rutinaDiaria();
		System.out.println("\n");

		System.out.println("Rutina diaria de un Fisico:\n");
		est = new Fisico();
		est.rutinaDiaria();
		System.out.println("\n");
	}
}