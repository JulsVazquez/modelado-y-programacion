public class Terrologo extends Estudiante{
	@Override
	void estudiar(){
		System.out.println("Estudiar (TT_TT). Veo rocas todo el " +
			"dia y no me gustan las matematicas.");

		if (Math.random() < 0.7)
			tieneTarea = true;
	}

	@Override
	void hacerTarea(){
		if(tieneTarea == true)
			System.out.println("Hacer tarea (TT_TT). Hago reportes " +
				"con las rocas que vimos hoy");
	}
}