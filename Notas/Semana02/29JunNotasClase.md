# Patrón Template

 El patron _Template_ define el esqueleto de un algoritmo en un método, 
 difiriendo en algunos pasos en sus subclases. Este deja a sus subclases 
 redefinir ciertas partes del algoritmo sin cambiar la estructura del mismo.
 
 El método de _template_ define los pasos de un algoritmo y permite que las
 subclases proporcionen la implementación de uno o más pasos.
 
Este patrón de diseño es uno de los más explotados en el desarrollo de software.

## Métodos Hooks

Tambien podemos tener métodos concretos que no hacen nada por defecto o 
implementación por defecto; llamamos a estos _hook_ (gancho). Esto da a las
subclases la capacidad de _engancharse_ al algoritmo en varios puntos si así lo
desean. Una subclase también es libre de ignorar el _gancho._

### ¿Cuándo usar hooks?

Cuando los métodos que son necesarios que las subclases implementen se utilizan 
métodos abtractos.

Cuando un método tiene una implementación general y no se requiere que todas las
subclases lo implementen de una manera distinta se utiliza un método hook.

# Patron State

El patron _state_ permite a un objeto alterar su comportamiento cuando su estado
interno cambia, el objeto aparenta cambiar su clase.

## Diferencia de State con Strategy

La diferencia es la intención:

* El patron de diseño _strategy_ se usa cuando tienes uno o varias clases que 
  comparten cierta similitud en su comportamiento, y se puede cambiar entre
  estos, pero sin importar un estado interno.
* El patron de diseño _state_ se usa cuando una clase tiene distintas fases
  (estados) y en cada una de estás su comportamiento es distinto.

# Revisión de código

* La revisión del código es un estudio cuidadoso y sistemático del código fuente
  por parte de personas que no son el autor original del código.

## Propósitos de la revisión

* Mejorar el código: Encontrar erroreres, anticipar errores, verificar la 
  claridad del código y verificar los estándares de estilo.
* Mejora del programador: Los programadores aprenden y se enseñan entre sí, 
  sobre las nuevas características del lenguaje, los cambios en el diseño del
  proyecto o sus estándares de codificación y las nuevas técnicas.

La revisiónde código se práctica ampliamiente en proyectos como Apache y Mozilla

Google no puede insertar ningún código en el repositorio principal hasta que
otro programador haya revisado y aprobado.

## Normas de estilo

La mayoría de las empresas tienen estándares de estilo de codificación.
Ejemplos: Google Java Style.

Pueden ser bastantes detallados, incluso hasta ek punto de especificar espacios
en blanco, sangrías, donde deben ir las llaves, etc.

Ejemplo

```
public class MuteQuack implements QUackBehavior {
    
    public void quack() {
        System.out.println("<<Silence >>");
    }
}
```

```
public class MuteQuack implements QuackBehavior
{
    public void quack()
    {
        System.out.println("<<Silence>>");
    }
}
```

Para Java hay una guía de estilo general.

Es importante ser autoconsistente y seguir las convenciones del proyecto en el
que se esta trabajando.

* El código duplicado es un riesgo para la seguridad.
* Copiar y pegar es peligroso, cuanto más largo sea el bloque más riesgoso es.

Los buenos comentarios deberían hacer que el código sea más fácil de entender, 
más seguro contra errores y listo para el cambio.

Un tipo de comentario crucial es una especificación, que aparece sobre un método
o sobre una clase y documenta el comportamiento del método o la clase.

En Java, eso se escribe convencionalmente como un comentario Javadoc, lo que 
significa que comienza con `/**` e incluye `@ -syntax`, como `@param` y 
`@return` para los métodos.

Un comentario crucial es uno que se especifica la procedencia o la fuente de un
fragmento de código que se copió o adoptó de otra parte.

Una de las razones es evitar violaciones de derechos de autor. 

Otra razón es que las fuentes pueden quedar desactualizados.

* Comentarios paja: Algunos comentarios son malos e innecesarios. Las 
  traducciones directas de código al inglés, no hacen nada apra mejorar la
  comprensión, porque debe asumir que su lector al menos concoe Java.

* El código oscuro debe obtener un comentario. 
  ```
  sendMessage("as you wish"); // this basically says "I love you"
  ```
  
También cuando los lenguajes te permiten comprimir varias instrucciones en una
sola, y simplemente no es fácil de deducir que es lo que esta haciendo esa línea
mágica.

* Failing fast: EL códgio debería revelar sus errores lo antes posible. 
  La comprobación estática falla más rápido que la comprobación dinámica, y la 
  comprobación dinámica falla más rápido que producir una respuesta incorrecta 
  que pueda dañar el cálculo posterior.
  
* No reutilizar parámetros y no reutilizar variables. Las variables no son un 
  recurso escaso.
  Darles buen nombre y dejar de usarlos cuando deje de necesitarlos. 
  Confundiriamos a nuestro lector si una variable que solía significar una cosa
  de repente comienza a significar algo diferente líneas abajo.
 
* Nombres de métodos y variables deben ser largos y autodescriptivos. Los 
  comentarios a menudo se pueden evitar por completo al hacer que el código sea
  más legible, con mejores nombres que describen los métodos y las variables.

    ```
    int tmp = 86400; //tmp es el número de segundos en un día (no hacer esto!!)
    int secondsPerDay = 86400; // (hacer esto)
    ```

* Los nombres largos y descriptivos haran que el código se lea claramente por sí
  mismos.

Seguir nomenclaturas léxicas del lenguaje.

```
* metodosAreNamedWithCamelCaseLikeThis
* variablesAreAlsoCamelCase
* CONSTANTES_ARE_IN_ALL_CAPS_WITH_UNDERCORES
* ClassesAreCapitalized
* packages.are.lowercase.and.separared.by.dots
```

* Los nombres de los métodos suelen ser frases verbales, como `getDate` o 
  `isUpperCase`, mientras los nombres de las variables y las clases suelen
  ser frases nominales. Hay que evitar las abreviaturas.
  
* Usar sangría consistente.

* Nunca usar carácteres de tabulación para la sangría, sólo los carácteres de 
  espacio.
  Sólo usar espacios y configurar el editor para insertar carácteres de espacio
  cuando presione la tecla Tab.

* Los métodos deben devolver resultados, no imprimirlos.
