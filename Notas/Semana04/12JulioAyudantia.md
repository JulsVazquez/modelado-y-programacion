Modelado y Programación Ayudatía del 12/Julio/2021
==================================================

Decorator y Adapter
-------------------

### Decorator
Permite añadir funcionalidad a un objeto existente sin alterar su estructura.
Los decoradores actúan como envolturas para los objetos de la clase original.
Las envolutas proveen funcionalidad adicional manteniendo su funcionalidad 
original intacta.

El patrón liga dinámicamente responsabilidades adicionales a un objeto. Los 
decoradores son una alternativa a las subclases para agregar funcionalidad.

Es posible que nuestro sistema quiera regresar a un estado anterior, quitarle
una capa.

Otra desventaja de esto, va creciendo el sistema de acuerdo a los decoradores.

### Adapter
Funciona como un puente entre dos interfaces incompatibles. Se combinan las 
capacidades de las interfaces independientes.

Adapter permite que un par de clases colaboren aunque tengan interfaces 
distintas. Como un adaptador de corriente real, servirá como intermediario entre
dos sistemas inamovibles que necesitan colaborar.

Problema que podría dar adapter es que si no esta bien integrado podría dar un
problema de rigidez. Dando sistemas inamovibles, ser rígidoz y viscoso.
