Modelado y Programación 15/Julio/2021
======================================

Builder/Prototype
---------------

### Builder
El patrón Builder encapsula la construcción de un producto para que este se
pueda construir en pasos.

* Iterator: Encapsula la iteración en un objeto separada ocultando 
  representación interna de la colección del cliente.

Filosofia: Encapsular la creación del builder y le solicitamos que construya la
estructura.

Beneficios: 
* Encapsula la forma en que se consturye un objeto complejo.
* Permite que los objetos se construyan en un proceso de varios pasos y 
  variable.
* Oculta la representación interna del producto del cliente.

Incovenientes:
* Requiere más conocimientos del dominio del cliente que cuando se usa Factory
* Debemos estar apegados a la estructura a modelar.
* A menuda se usa para construir estructuras compuestas.

* Builder: 
 -Interfaz abstracta para crear productos.
* Concrete Builder: 
  - Implemetación del Builder. 
  - Construye y teune las partes necesarias para construir los productos.
* Director: 
  - Construye un objeto usando el patrón Builder.
  - ...

### Prototype
Se usa cuando creamos una instancia de una clase que determina el costo o lo 
complica.

Permite hacer nuevas instancias copiando instancias existentes o deserialización
cuando se necesitan copias profundad.

El código del cliente puede crear nuevas instancias.

_Paso por valor, paso por referencia._
_La clonación es paso por valor._
* Cliente: Es el encargado de solicitar la creación de los nuevos objetos a 
  partir de los prototipos.
* Prototipo: Declara una interfaz para clonarse, a la que accede el cliente.
* Prototipo Contreto: ...

_Visitar la documentación de Cloneable._

* Beneficios: 
  - Clonar un objeto es mucho más rápido que crearlo.
  - Un programa puede añadir y borrar dinámicamente objetos prototipos
* Desventajas:
  - La creación de nuevos objetos acarrea un coste computacional elevado.
  - Los objetos a crear tienen o suelen tener atributos que repiten su valor.
  - Una desventaja de usar el prototipo es que hacer una copia puede ser 
    complicado.
  - En objetos muy complejos, implementarlo puede ser complicado.
