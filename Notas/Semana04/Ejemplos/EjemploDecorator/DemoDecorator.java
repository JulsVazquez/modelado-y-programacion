public class DemoDecorator{
	public static void main(String[] args){
		
		Personaje caballero = new Caballero();
		Personaje troll = new Troll();

		System.out.println(caballero.getDescripcion() + 
			" ataque: " + caballero.getAtaque() +
			" defensa: " + caballero.getDefensa());

		System.out.println(troll.getDescripcion() + 
			" ataque: " + troll.getAtaque() +
			" defensa: " + troll.getDefensa());

		System.out.println("\nLos personajes armados: ");

		caballero = new ConEspada(caballero);
		caballero = new ConEscudo(caballero);
		caballero = new ConArmadura(caballero);
		System.out.println(caballero.getDescripcion() + 
			" ataque: " + caballero.getAtaque() +
			" defensa: " + caballero.getDefensa());

		troll = new ConHacha(troll);
		System.out.println(troll.getDescripcion() + 
			" ataque: " + troll.getAtaque() +
			" defensa: " + troll.getDefensa());

	}
}