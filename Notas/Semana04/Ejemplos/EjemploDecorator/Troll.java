public class Troll extends Personaje{
	
	public Troll(){
		descripcion = "RADARADARADA!!(Soy un troll)";
		ataque = 10;
		defensa = 5;
	}

	@Override
	public int getAtaque(){
		return ataque;
	}

	@Override
	public int getDefensa(){
		return defensa;
	}
}