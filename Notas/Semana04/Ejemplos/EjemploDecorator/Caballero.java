public class Caballero extends Personaje{
	
	public Caballero(){
		descripcion = "Soy un caballero";
		ataque = 5;
		defensa = 0;
	}

	@Override
	public int getAtaque(){
		return ataque;
	}

	@Override
	public int getDefensa(){
		return defensa;
	}
}