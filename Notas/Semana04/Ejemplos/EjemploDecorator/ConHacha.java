public class ConHacha extends Personaje{
	Personaje personaje;

	public ConHacha(Personaje personaje){
		this.personaje = personaje;
	}

	@Override
	public String getDescripcion(){
		return personaje.getDescripcion() + " + hacha";
	}

	@Override
	public int getAtaque(){
		return personaje.getAtaque() + 7;
	}

	@Override
	public int getDefensa(){
		return personaje.getDefensa() + 0;
	}
}