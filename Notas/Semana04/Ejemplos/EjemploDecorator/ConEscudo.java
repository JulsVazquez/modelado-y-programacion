public class ConEscudo extends Personaje{
	Personaje personaje;

	public ConEscudo(Personaje personaje){
		this.personaje = personaje;
	}

	@Override
	public String getDescripcion(){
		return personaje.getDescripcion() + " + escudo";
	}

	@Override
	public int getAtaque(){
		return personaje.getAtaque() + 0;
	}

	@Override
	public int getDefensa(){
		return personaje.getDefensa() + 5;
	}
}