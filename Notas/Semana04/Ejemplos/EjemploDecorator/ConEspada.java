public class ConEspada extends Personaje{
	Personaje personaje;

	public ConEspada(Personaje personaje){
		this.personaje = personaje;
	}

	@Override
	public String getDescripcion(){
		return personaje.getDescripcion() + " + espada";
	}

	@Override
	public int getAtaque(){
		return personaje.getAtaque() + 5;
	}

	@Override
	public int getDefensa(){
		return personaje.getDefensa() + 0;
	}
}