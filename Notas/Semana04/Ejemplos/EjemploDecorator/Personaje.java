public abstract class Personaje{
	String descripcion;
	int ataque = 0;
	int defensa = 0;

	public String getDescripcion(){
		return descripcion;
	}

	public abstract int getAtaque();

	public abstract int getDefensa();
}