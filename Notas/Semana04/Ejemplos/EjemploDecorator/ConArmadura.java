public class ConArmadura extends Personaje{
	Personaje personaje;

	public ConArmadura(Personaje personaje){
		this.personaje = personaje;
	}

	@Override
	public String getDescripcion(){
		return personaje.getDescripcion() + " + armadura";
	}

	@Override
	public int getAtaque(){
		return personaje.getAtaque() + 0;
	}

	@Override
	public int getDefensa(){
		return personaje.getDefensa() + 10;
	}
}