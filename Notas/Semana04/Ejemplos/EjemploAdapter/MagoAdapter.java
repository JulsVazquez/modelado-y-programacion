public class MagoAdapter implements Personaje{
	
	public Mago mago;

	public MagoAdapter(Mago mago){
		this.mago = mago;
	}

	@Override
	public void atacar(){
		mago.conjurar();
	}

	@Override
	public void defender(){
		mago.proteger();
	}

	@Override
	public void escapar(){
		mago.abrirPortal();
	}
}