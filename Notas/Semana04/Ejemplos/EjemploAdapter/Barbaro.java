public class Barbaro implements Personaje{

	@Override
	public void atacar(){
		System.out.println("El barbaro ataca con hacha.");
	}

	@Override
	public void defender(){
		System.out.println("El barbaro resiste las heridas.");
	}

	@Override
	public void escapar(){
		System.out.println("El barbaro escapa corriendo.");
	}
}