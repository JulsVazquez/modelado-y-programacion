public class DemoAdapter{
	
	public static void main(String[] args){
		Personaje barbaro = new Barbaro();
		Personaje caballero = new Caballero();
		
		Mago mago = new Mago();
		Personaje magoAdaptado = new MagoAdapter(mago);

		System.out.println("\n----------Acciones del barbaro----------");
		barbaro.atacar();
		barbaro.defender();
		barbaro.escapar();

		System.out.println("\n----------Acciones del caballero----------");
		caballero.atacar();
		caballero.defender();
		caballero.escapar();

		System.out.println("\n----------Acciones del mago----------");
		magoAdaptado.atacar();
		magoAdaptado.defender();
		magoAdaptado.escapar();
	}
}