public class Caballero implements Personaje{

	@Override
	public void atacar(){
		System.out.println("El caballero ataca con espada.");
	}

	@Override
	public void defender(){
		System.out.println("El caballero se defiende con escudo.");
	}

	@Override
	public void escapar(){
		System.out.println("El caballero monta su caballo y huye.");
	}
	
}