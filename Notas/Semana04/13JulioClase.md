Modelado y Programación 13/Julio/2021
=====================================

Factory method y Abstract Factory
---------------------------------

### Factory method

Tenemos que identificar lo que cambia de lo que no y encapsularlo.

Principios de diseño:
* Identificar los aspectos...
* ....

Las fabricas deben manejar los detalles de la creaciónde objetos.

Simple Factory o Factory suele considerarse un modismo de programación.

Algunos desarrolladores confunden este patrón con el Patron de 
"Abstract Factory".

El patrón de método de fábrica encapsula la creación de objeetos permitiendo la
creación de objetos permitiendo que las subclases decidan qué objetos crear.

* Creamos una clase abstracta que es nuestra fabrica.
* Dentro de esta declaramos el método general donde se hacen las actividades en 
  común y dentro de este mismo método llamamos métodos de actividades que 
  cambian
* Sus descendientes...

Hay que depender de la abstracción y no de la clase concreta.
Principio de inversión de dependencia.

### Abstract Factory
Proporcionar un medio para crar una familia de fabricas.
Una fabrica abstracta nos da una interfaz...

Eso nos permite implementar varidad de fabricas que producen...
Debido a que nuestro código está desacoplado de los productos reales, podemos
sustituir diferentes fábricas para obtener diferentes comportamientos.

Abstract Factory proporciona....
