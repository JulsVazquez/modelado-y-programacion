# Notas de la clase 04 de Marzo del 2021

# Patrones de Diseño
Existen varios problemas al hacer un diseño de programa:
* Rígidez: Estar cambiando el código tantas veces que no es cómo la original
* Fragilidad: Que en algunos momentos el programa falla. 
* Inmovilidad: Cuando nuestro código no puede ser móvil, no permite utilizar la
parte que nosotros requerimos.
* Viscosidad: Puede haber viscosidad en software y entorno.
  - Software: Si hacemos un proyecto con cierto patrón y cómo siempre hay 
  cambios y rompemos la estructura del código, rompiendo con ello el patrón.
  - Entorno: Cuando tarda en compilar un programa demasiado tiempo. Existe un
  entorno de desarrollo lento o ineficiente.
  
¿Qué podemos hacer para no caer en estos diseños? o ¿Qué existe para ayudarnos?

Basarnos en patrones que han resultado eficientes.

Los patrones de diseño son descripciones de clases y objetos para resolver un 
diseño general en un contexto determinado.

Revisar "Design Patterns: Elements of Reusable Object Oriented Software". En 
este libro catalogan 23 patrones y se destacan estrategias y aproximaciones
basadas en estos diseños.

# Clasificación de patrones (GoF)
* Patrones de creación
* Patrones estructurales
* Patrones de comportamiento

¿Qué hacen los patrones de creación?

* Abstraen el proeso de instanciación haciendo al sistema independiente de como
los objetos son creados, compuestos o representados.
* Alternativas de diseño por herencia.
* Encapsulan el mecanismo de creación.
* Independencia de los tipos de objetos "producto" que manejamos.

# Patrones estructurales
* Determinan como combinar objetos y clases para definir estructuras complejas.
 - Comunicar dos clases incompatibles
 - Añadir funcionalidad a objetos que son utiles pero les faltan cosas.
 
* Adapter
* Bridge
* Composite
* Decorator
* etc.

# Patrones de comportamiento
* Se ocupan de los algoritmos y reparto de responsabilidades.
* Describen los patrones de comunicación entre objetos y clases.
* Podremos definir abstracciones de algoritmos (Template Method)
* Cooperaciones entre objetos para realizar tareas complejas, reduciendo las 
dependencias entre objetos (Iterator, Observer, etc)
* Asociar comportamiento a objetos e invocar su ejecución 
(Command, Strategy, etc)

* Cadena de responsabilidades
* Interpreter
* Interator
* Strategy

# Otros tipos de patrones
* Patrones de programación concurrente
* Patrones de interfaz gráfica
* Patrones de organización, optimización y robustez de código.
* Patrones guiados por pruebas

¿Por qué usar patrones de diseño?

* Se definen con un alto nivel de abstracción
* Son independientes de los lenguajes de programación y de los detalles de 
implementación
* Proporcionan un esquema para refinar los subsistemas o componentes de un 
sistema de software, o las relaciones entre ellos.

Beneficios de los patrones
* Los patrones favorecen la reutilización de diseño y arquitecturas a gran
escala
* Capturan el conocimiento de los expertos y lo hacen accesible a toda la 
comunidad de software
* Estalecen terminologías en común. Los nombres de los patrones forman parte
del vocabulario técnico del ingeniero de software.
* Usandolos logramos una estructura **flexible, mantenible y reutilizable**

# Problema de los patrones
Puede que existan problemas al implementar los patrones.
* La integración de los patrones se hace de manera manual
* El número de patrones identificados es cada vez más grande. Las 
clasificaciones actuales no siempre sirven de guía para decidir cual usar
* El número de combinaciones de patrones, estilos y atributos que se dan en la
práctica son incontables
* Los patrones se validan por la experiencia y el debate, no mediante la 
aplicación de técnicas formales

# Cambios en el proceso de desarrollo
Los cambios en un diseño de software no son previstos en el diseño original, se
degradan el mismo software.
* Generalmente lo hacen las personas que no estaban relacionadas con la 
filosofía de diseño original.

# ¿Cómo usar los patrones de diseño?
- Tener una visión general de los patrones
- Volver y estudiar la estructura del patrón a implementar
- Ver un ejemplo concreto códificado de ese patron
- Elegir nombres significativos en el contexto del patrón
- Implementar el patrón

