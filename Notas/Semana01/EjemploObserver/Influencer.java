import java.util.LinkedList;

public class Influencer implements Sujeto{

	String nombre;
	LinkedList<Seguidor> seguidores = new LinkedList<Seguidor>();
	String estado;

	public Influencer(String nombre){
		this.nombre = nombre;
	}

	public String getNombre(){
		return nombre;
	}

	public LinkedList<Seguidor> getSeguidores(){
		return seguidores;
	}

	public String getEstado(){
		return estado;
	}

	public void setEstado(String nuevoEstado){
		estado = nuevoEstado;
		System.out.println("\n" + nombre + ". Nuevo estado: " + estado);
	}

	public void registrar(Seguidor s){
		seguidores.add(s);
	}

	public void remover(Seguidor s){
		System.out.println("\n" + nombre + ", has perdido al seguidor: " + s.getUsuario());
		seguidores.remove(s);
	}
	
	public void notificar(){
		for(Seguidor s : seguidores){
			s.update();
		}
	}
}
