public class Main{
	public static void main(String[] args){
		Influencer millennial = new Influencer("Millenial");

		millennial.setEstado("#yolo");
		millennial.notificar();

		System.out.print("\n");
		Seguidor s1 = new Seguidor("fulanito1", millennial);
		Seguidor s2 = new Seguidor("mengana2", millennial);
		Seguidor s3 = new Seguidor("polifacia3", millennial);
		Seguidor s4 = new Seguidor("papadopolus4", millennial);

		for(Seguidor s : millennial.getSeguidores()){
			System.out.println("Te sigue el usuario: " + s.getUsuario());
		}

		millennial.setEstado("#TRUMP");
		System.out.print("\n");
		millennial.notificar();

		s3.dejarDeSeguir();

		millennial.setEstado("#hashtag");
		System.out.print("\n");
		millennial.notificar();

	}
}