# Notas del vídeo de Strategy

La herencia tiene las siguientes desventajas:
* Código duplicado a través de las subclases.
* Dificil conocer la información sobre el comportamiento de todos los objetos
* No podemos hacer que los objetos hagan otras acciones.
* Los objetos no pueden realizar dos acciones al mismo tiempo.
* Los cambios pueden afectar involuntariamente a otros objetos.

Siempre hay cambios a lo largo del desarrllo de software, sin importan que tan
bien este diseñado con el tiempo debe de crecer y cambiar, sino morirá.

# Principios de diseño
Identificar los aspectos de que varían encapsulandolos y separarlos de los que 
no cambian. Tomar lo que varía y _encapsularlo_ para que no afecte al resto de 
nuestro código.

En el problema de la clase *Pato*, los métodos `volar()` y `graznar()` son 
comportamientos de la clase que varían según el tipo de patito.

**_Programar a una interfaz y no a una implementación_**

A partir de ahora, los comportamientos vivirán en una clase separada, una clase
que implementa una interfaz de comportamiento particular.

De esta forma, las clases de Pato no necesitarán conocer ningún detalle de 
implementación para sus propios comportamientos.

_Programar una interfaz_ significa programar un _super tipo_.

Programar una implementación sería:
        
    Dog d = new Dog();
    d.bark();
    
Pero programar una interfaz/supertipo:

    Animal animal = new Dog();
    animal.makeSound();

    
    +------------------------+
    |         Animal         |
    |------------------------+
    | makeSound()            |
    |________________________+
    
Y sus clases que apuntan a la clase `Animal` serían:

    +------------------------+
    |         Dog            |
    |------------------------+
    | makeSound(){           |
    |   bark();              |
    | }                      |
    | bark(){//bark sound    |
    |________________________+
    
    +------------------------+
    |         Cat            |
    |------------------------+
    | makeSound(){           |
    |   meow();              |
    | }                      |
    | meow(){//meow sound    |
    |________________________+
    

Favorecer la composición sobre la herencia.

Crear sistemas con composición que le da mucha más flexibilidad. No sólo 
permitimos encapsular una familia de algoritmos en su propio conjunot de 
clases, sino que también le permite cambiar el comportamiento en tiempo de
ejecución siempre que el objeto con el que está redactando implemente la 
interfaz de comportamiento correcta.

# Strategy
El patrón de estrategia define una familia de algoritmos, encapsula cada uno y
los hace intercambiables. Permite que el algoritmo varie independientemente de 
los clientes.

# Observer
En el primer ejemplo que vemos falta lo siguiente:
* Encapsular lo que cambiará constantemente.
* No existe una manera de agregar o eliminar los elementos que se visualizarán 
  sin necesidad de modificar el código.
* Al menos parece que estamos usando una interfaz común para hablar con los 
  elementos de la plabtalla, todos tienen un método `update()` que toma los
  valores de temperatura, húmedad y presión

_Observer_ define una dependencia de "uno a muchos" entre objetos de tal forma
que cuando un objeto cambia de estado, todos sus dependientes notificados y se
actualizan automáticamente.

* El objeto matiene el estado y manda los cambios a los objetos dependientes. Lo 
cual lo veulve una relación uno a muchos.
* Los observadores son dependientes del sujeto, ya que cuando el estado del
  sujeto cambia, los observadores son notificados.
* El observador puede también ser actualizado con nuevos valores.

Implementando _Observer_ tenemos que:
* Lo único que un sujeto sabe de un observador es que implementa una interfaz
  determinada.
* Se pueden agregar observadores en cualquier momento.
* No es necesario modificar al sujeto para agregar nuevos observadores.
* Los cambios entre el sujeto u observador no afectan al otro.

_Esforzarce por obtener diseños débilmente acoplados entre objetos que 
interactúen._

_Los diseños poco acoplados nos permiten construir sistemas Orientados a Objetos
más felxibles que pueden manejar el cambio porque minimizan la interdependencia
entre los objetos._
