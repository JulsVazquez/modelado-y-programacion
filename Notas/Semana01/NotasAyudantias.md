# Notas del Laboratorio de Modelado 08/Marzo/2021

# UML
Lenguaje de modelado visual que se usa para especificar, visualizar, construir y
documentar artefactos de un sistema de software.

Modela un sistema desde varios puntos de vista separados pero relacionados.

Ayuda a diseñar, mantener, entender y configurar un sistema.

* Área estructural
 - Vista estática - diagrama de clases
 - Vista de casos de uso - diagrama de casos de uso

* Área dinámica
 - Vista de máquina de estado - diagrama de estados
 - Vista de actividad - diagrama de actividades
 - Vista de interacción - diagrama de secuencia

# Diagrama de clases
Perspectiva estructural del sistema. ¿Cómo se organizan las clases?
* Una clase esta representada por un rectangulo.
* En la segunda sección están los atributos.
* Y en el tercero están los métodos

        +-----------------------------+
        |                             |
        |      Nombre de la Clase     |
        |_____________________________|
        |                             |
        | atributo1: tipo             |
        | atributo2: tipo             |
        | ...                         |
        |_____________________________|
        |                             |
        |métoodo1(parametro:tipo):tipo|
        |método2(): void              |
        |....                         |
        +-----------------------------+
        
        A ----------------- B (A se asocia con B)

        A <>----------------B (A tiene un objeto de tipo B)

        A <|----------------B (B hereda a A)

        A <- - - - - - - - -B (B implementa a A)

        <<interfaz>>        <<Abstract>>

Esas son las etiquetas.

# Diagrama de casos de uso
Perspectiva de los requerimientos que un sistema debe cumplir.

* Contiene un actor: No todos los actores realizan la misma actividad.
* Relación de uso (A usa a B)
* Relación de extensión (D extiende de C)
* Relación de herencia (E hereda de F)

# Diagrama de estados
Perspectiva del comportamiento dinámico de los objetos.

Modela el ciclo de vida de los objetos de una clase.

# Nota Ayudantía 10/Marzo/2021

# Diagrama de actividades
Perspectiva del flujo de trabajo de un caso de uso.

Representa un sólo caso de uso.

Cada estado representa el cómputo de una acción.

# Diagrama de secuencias
Perspectiva de la interacción entre objetos de distintas clases de forma
colaborativa a través de mensajes.

Los objetos tienen un tiempo de vida.

La parte izquierda del diagrama lleva un Actor.

### Stretegy
El comportamiento de una clase cambia en tiempo de ejecución. Se crea una 
familia debealgoritmos que representan varias estrategias para resolver un 
problema y un objeto de contexto que cambia su comportamiento respecto a la 
estrategia. EL patrón permite que el algoritmo cambie independientemente.

Desventaja: Muchas clases asociadas a un mismo objeto. Cuando son muchisimas
tareas tu tiempo de ejecución no es muy eficiente

### Observer
Define una relación de 1 a muchos entre un conjunto de objetos. Cuando el 
estado de un _Sujeto_ cambia sus observadores son notificados.
